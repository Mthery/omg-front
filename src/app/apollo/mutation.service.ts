import {Apollo} from "apollo-angular";
import gql from "graphql-tag";
import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class MutationService {

  constructor(private apollo: Apollo, private http: HttpClient) {
  }

  private getMutation(mutationName: string, queryFields: any[], argumentFields: {key: string, type: string, value: any}[]) {
    let mutation = 'mutation(';
    argumentFields.forEach(argument => {
      mutation += '$' + argument.key + ': ' + argument.type;
      mutation += argument.type.indexOf('!') >= 0 ? ', ' : '!, ';
    });
    mutation += ') {' + mutationName + '(';
    argumentFields.forEach(argument => {
      mutation+= argument.key + ': $' + argument.key + ', ';
    })
    mutation += ')';

    if (queryFields) {
      mutation += '{' + queryFields.join(', ') + '}';
    }

    mutation += '}';
    return mutation;
  }

  public mutateSingleRow(mutationName: string, queryFields: any[] = null, argumentFields: {key: string, type: string, value: any}[]) {
    let mutation = this.getMutation(mutationName, queryFields, argumentFields);
    let variables = {};
    argumentFields.forEach(argument => {
      variables[argument.key] = argument.value;
    })

    return this.apollo.mutate({
      mutation: gql(mutation),
      variables: variables,
      context: {
        useMultipart: true,
      },
    })
      .toPromise()
  }

  public mutateSingleRowWithUpload(mutationName: string, queryFields: any[] = null, argumentFields: {key: string, type: string, value: any}[]) {
    let variables = {};
    let _map = {};
    argumentFields.forEach(argument => {
      variables[argument.key] = argument.value;
      _map[argument.key] = ["variables." + argument.key];
    })
    const operations = {
      query: this.getMutation(mutationName, queryFields, argumentFields),
      variables: variables
    }
    const fd = new FormData();
    fd.append('operations', JSON.stringify(operations));
    fd.append('map', JSON.stringify(_map));
    argumentFields.forEach(argument => {
      fd.append(argument.key, argument.value);
    });
    return this.http.post('http://localhost:8080/graphql', fd).toPromise();
  }

}
