import {Apollo} from "apollo-angular";
import gql from "graphql-tag";
import {FieldBase} from "src/app/models/field-base";
import {Injectable} from "@angular/core";

const QUERY_SINGLE: string = "query($id: Int!) {";
const QUERY_MANY = 'query';

export interface queryOptions {
  searchFilter?: string;
  sortFieldFilter?: string;
  sortDirectionFilter?: string;
}

@Injectable()
export class QueryService {

  constructor(private apollo: Apollo) {
  }

  public querySingleRow(queryName: string, queryFields: FieldBase[], queryId: string) {
    const query = gql(QUERY_SINGLE +
      queryName + '(id: $id) {' +
      queryFields.map(field => {
        if (field.queryOptions && field.queryOptions.sub_selection) {
          return field.key + '{' + field.queryOptions.sub_selection.map(field => {
              if (field.queryOptions && field.queryOptions.sub_selection) {
                return field.key + '{' + field.queryOptions.sub_selection.map(field => field.key ? field.key : field).join() + '}';
              } else {
                return field.key;
              }
            }
          ).join() + '}';
        } else {
          return field.key;
        }
      }).join(', ') + '}}');
    return this.apollo.query({
      query: query,
      variables: {
        id: queryId
      },
      fetchPolicy: "network-only"
    })
      .toPromise();
  }

  public queryManyRows(queryName: string, queryFields: FieldBase[], options?: queryOptions) {
    const query = gql(QUERY_MANY + (options ? "($options: Options) { " : " { ") +
      queryName +  (options ? "(options: $options)" : "") + '{ ' +
      queryFields.map(field => {
        if (field.queryOptions && field.queryOptions.sub_selection) {
          return field.key + '{' + field.queryOptions.sub_selection.map(field => {
              if (field.queryOptions && field.queryOptions.sub_selection) {
                return field.key + '{' + field.queryOptions.sub_selection.map(field => field.key ? field.key : field).join() + '}';
              } else {
                return field.key;
              }
            }
          ).join() + '}';
        } else {
          return field.key;
        }
      }).join(', ') + '}}');
    return this.apollo.query({
      query: query,
      variables: {
        options: options
      },
      fetchPolicy: "network-only"
    })
      .toPromise();
  }

  public queryManyRowsWithoutArgs(queryName: string, queryFields: string[]) {
    const query = gql(QUERY_MANY + " { " +
      queryName + '{ ' + queryFields.join(', ') + '}}');
    return this.apollo.query({
      query: query,
      fetchPolicy: "network-only"
    })
      .toPromise();
  }

  public queryManyRowsWithArgs(queryName: string, queryFields: FieldBase[], argumentFields: { key: string, type: string, value: any }[]) {
    let query = 'query(';
    argumentFields.forEach(argument => {
      query += '$' + argument.key + ': ' + argument.type;
      query += argument.type.indexOf('!') >= 0 ? ', ' : '!, ';
    });
    query += ') {' + queryName + '(';
    argumentFields.forEach(argument => {
      query += argument.key + ': $' + argument.key + ', ';
    })
    query += ') {' + 
    queryFields.map(field => {
      if (field.queryOptions && field.queryOptions.sub_selection) {
        return field.key + '{' + field.queryOptions.sub_selection.map(field => {
            if (field.queryOptions && field.queryOptions.sub_selection) {
              return field.key + '{' + field.queryOptions.sub_selection.map(field => field.key ? field.key : field).join() + '}';
            } else {
              return field.key;
            }
          }
        ).join() + '}';
      } else {
        return field.key;
      }
    }).join(', ') + '}}';

    let variables = {};
    argumentFields.forEach(argument => {
      variables[argument.key] = argument.value;
    })

    return this.apollo.query({
      query: gql(query),
      variables: variables,
      fetchPolicy: "network-only",
    })
      .toPromise()
  }

}
