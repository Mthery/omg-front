export enum Levels {
  'X < 2' = "Débutant (0 à 2 ans)",
  '2 < X < 4' = "Intermédiaire (2 à 4 ans)",
  '4 < X < 8' = "Confirmé (4 à 8 ans)",
  '8 < X' = "Sénior (plus de 8 ans)"
}