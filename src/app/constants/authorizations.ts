export enum Authorizations {
  createMission = 'MANAGER,IC,HR,ADMIN',
  deleteMission = 'MANAGER,IC,HR,ADMIN',
  duplicateMission = 'MANAGER,IC,HR,ADMIN',
  editMission = 'MANAGER,IC,HR,ADMIN',
  createPerson = 'MANAGER, IC, HR, ADMIN',
  editPerson = 'MANAGER, IC, HR, ADMIN',
}
