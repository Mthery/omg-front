export enum Status {
  CLOSED = 'AO fermé',
  CRITICAL = 'Critique/Perte de position',
  LOSS = 'AO perdu',
  NEED = 'Besoin constant',
  'NEW-WON' = 'Nouveau projet gagné',
  OPEN = 'AO ouvert',
  'RECRUITING-GROUND' = 'Vivier/avance de phase',
  UNANSWERED = 'AO non répondu',
  REPLACEMENT = 'Remplacement suite démission',
  SENT = 'Réponse envoyée',
  SWITCH = 'Besoin pourvu',
  WIN = 'AO gagné'
}