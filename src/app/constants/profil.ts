export enum Profil {
  CANDIDATE = 'Candidat',
  CONTACT = 'Contact',
  FREELANCE = 'Freelance',
  CONTRACTOR = 'Contractuel',
  INTERN = 'Collaborateur',
  MANAGER = 'Manager',
  IC = 'Ingénieur commercial',
  HR = 'Ressources humaines',
  ADMIN = 'Administrateur'
}
