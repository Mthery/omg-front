export enum Priorities {
  P1 = 'Priorité 1',
  P2 = 'Priorité 2',
  P3 = 'Priorité 3'
}