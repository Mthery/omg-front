import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import { AdminComponent } from 'src/app/admin/admin.component';
import { IsAdminGuard } from 'src/app/guards/is-admin.guard';
import { IsSignedInGuard } from 'src/app/guards/is-signed-in.guard';
import { MissionsComponent } from 'src/app/missions/missions.component';
import { NotFoundComponent } from 'src/app/not-found/not-found.component';
import { ProfilComponent } from 'src/app/profil/profil.component';
import { UsersComponent } from 'src/app/users/users.component';
import { WelcomePageComponent } from 'src/app/welcome-page/welcome-page.component';

const routes: Routes = [
  { path: 'admin', component: AdminComponent, canActivate: [IsAdminGuard] },
  { path: 'offres', component: MissionsComponent, canActivate: [IsSignedInGuard] },
  { path: 'profil', component: ProfilComponent, canActivate: [IsSignedInGuard] },
  { path: 'users', component: UsersComponent/*, canActivate: [IsSignedInGuard]*/ },
  { path: '', component: WelcomePageComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
