import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { SetTokenInterceptor } from 'src/app/interceptors/http-interceptor';

export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: SetTokenInterceptor, multi: true },
];