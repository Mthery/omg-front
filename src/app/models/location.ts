import {Client} from 'src/app/models/client';
import {Person} from "./person";

export class ClientLocation {
  clientId: number;
  id: number;
  label: string;

  constructor(l: Location, cId: number) {
    this.clientId = cId;
    this.id = l.id;
    this.label = l.label;
  }
}

export interface Location{
  client: Client;
  id: number;
  label: string;
  missions: number[];
  person: Person;
}

export class LocationCreate {
  client_id: number
  label: string;

  constructor(l: ClientLocation) {
    this.client_id = l.clientId;
    this.label = l.label;
  }
}

export class LocationUpdate {
  id: number;
  label: string;

  constructor(l: ClientLocation) {
    this.id = l.id;
    this.label = l.label;
  }
}