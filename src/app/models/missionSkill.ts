import { Mission } from 'src/app/models/mission';
import { Skill } from 'src/app/models/skill';

export interface MissionSkill {
  level: string;
  mandatory: boolean;
  id: number;
  mission: Mission;
  skill: Skill;
}

export class MissionSkillRequest {
  level: string;
  mandatory: boolean;
  mission_id: number;
  skill_id: number;

  constructor(ms: MissionSkill) {
    this.level = ms.level;
    this.mandatory = ms.mandatory;
    this.mission_id = ms.mission.id;
    this.skill_id = ms.skill.id;
  }
}

export class MissionSkillSuggestion {
  level: string;
  mandatory: boolean;
  skill_id: number;

  constructor(ms: MissionSkillRequest) {
    this.level = ms.level;
    this.mandatory = ms.mandatory;
    this.skill_id = ms.skill_id;
  }
}