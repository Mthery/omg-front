export default interface BottomSheetResponse {
  message: string;
}