import { Location } from 'src/app/models/location';

export class Client {
  id: number;
  label: string;

  constructor(c: ClientVM) {
    this.id = c.id;
    this.label = c.label;
  }
}

export class ClientVM {
  id: number;
  label: string;
  location: Location[];
  nbUsed: number;

  constructor(id: number, label: string, location: Location) {
    this.id = id;
    this.label = label;
    this.location = [location];
    this.nbUsed = location.missions ? location.missions.length : 0;
  }
}

export class ClientRequest {
  id: number;
  label: string;
  location: Location[];

  constructor(client: ClientVM) {
    this.label = client.label;
    this.location = client.location;
  }
}