export class FieldBase {
  key: string;
  queryOptions: QueryOptions;

  constructor(key?: string, queryOptions?: QueryOptions) {
    this.key = key;
    this.queryOptions = queryOptions || null;
  }

}

export interface QueryOptions {
  sub_selection: { key: string, queryOptions: QueryOptions }[]
}

export interface Section {
  create: string;
  createMultiple?: string;
  delete?: string;
  detail?: string;
  edit?: string;
  fields?: FieldBase[];
  search?: string;
}
