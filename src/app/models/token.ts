export default interface Token {
  exp: number;
  jti: number;
  userRole: string;
}