import { Client } from 'src/app/models/client';
import ClientLocation from 'src/app/models/clientLocation';
import { MissionSkill } from 'src/app/models/missionSkill';
import { Person } from 'src/app/models/person';

interface BaseMission {
  adr: string;
  agency: string;
  contact_email: string;
  contact_phone: string;
  date_out: string;
  date_start: string;
  description: string;
  priority: string;
  skills: MissionSkill[];
  status: string;
  type: string;
}

export interface Mission extends BaseMission {
  candidates: Person[];
  client: Client;
  client_location: ClientLocation;
  created_by: Person;
  id: number;
  replaced: Person;
}

export class MissionCandidate {
  collaborator_id: number;
  mission_id: number;

  constructor(cId: number, mId: number) {
    this.collaborator_id = cId;
    this.mission_id = mId;
  }
}

export interface MissionRequest extends BaseMission {
  client_location_id: number;
  created_by: number;
  date_in: string;
  replaced: number;
}

export class MissionVM {
  agency: string;
  candidates: string[];
  client: string;
  clientLocation: string;
  createdBy: string;
  description: string;
  id: number;
  priority: string;
  skills: string[];
  startDate: string;
  status: string;
  stopDate: string;
  type: string;

  constructor(mission: Mission, status: string, type: string, priority: string) {
    this.agency = mission.agency;
    this.candidates = mission.candidates.map(c => `${c.lastname} ${c.firstname}`);
    this.client = mission.client.label;
    this.clientLocation = mission.client_location.label;
    this.createdBy = `${mission.created_by.firstname} ${mission.created_by.lastname}`;
    this.description = mission.description;
    this.id = mission.id;
    this.priority = priority;
    this.skills = mission.skills.map(s => s.skill.label);
    this.startDate = mission.date_start;
    this.status = status;
    this.stopDate = mission.date_out;
    this.type = type;
  }
}