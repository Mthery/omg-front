export interface Skill {
  id: number;
  label: string;
  missionSkills: number[];
}

export class SkillUpdate {
  id: number;
  label: string;

  constructor (s: Skill) {
    this.id = s.id;
    this.label = s.label;
  }
}