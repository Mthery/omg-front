export interface CandidatesSuggestionsData<T> {
  candidatesSuggestions: T;
}

export interface CreateMissionData {
  createMission: number;
}

export interface CollaboratorsData<T> {
  collaborators: T;
}

export interface DuplicateMissionData {
  duplicateMission: number;
}

export interface LocationsData<T> {
  locations: T;
}

export interface MissionData<T> {
  mission: T;
}

export interface MissionsData<T> {
  missions: T;
}

export interface MissionSkillsData<T> {
  missionSkills: T;
}

export interface PersonData<T> {
  person: T;
}

export interface PersonByNameData<T> {
  personByName: T;
}

export interface PersonsData<T> {
  persons: T;
}

export interface SkillsData<T> {
  skills: T;
}
