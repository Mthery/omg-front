export default interface ClientLocation {
  id: number;
  label: string;
}