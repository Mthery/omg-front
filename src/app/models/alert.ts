import { MissionSkillSuggestion, MissionSkillRequest } from 'src/app/models/missionSkill';
import { Person } from 'src/app/models/person';
import { ClientVM } from 'src/app/models/client';

export class CandidateAlertData {
  candidates: Person[];
  missionSkills: MissionSkillSuggestion[];
  readOnly: boolean;

  constructor(p: Person[], ms: MissionSkillSuggestion[], ro: boolean) {
    this.candidates = p;
    this.missionSkills = ms;
    this.readOnly = ro;
  }
}

export class SkillAlertData {
  missionSkills: MissionSkillRequest[];
  readOnly: boolean;

  constructor(ms: MissionSkillRequest[], ro: boolean) {
    this.missionSkills = ms;
    this.readOnly = ro;
  }
}

export class ClientAlertData {
  client: ClientVM;
  idsToDelete: number[];

  constructor(client: ClientVM, ids: number[]) {
    this.client = client;
    this.idsToDelete = ids;
  }
}