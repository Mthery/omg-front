export interface Candidate {
  firstname: string;
  id: number;
  lastname: string;
  percentage: number;
}

export class EditPerson {
  lastname: string;
  firstname: string;
  email: string;
  phone: string;

  constructor(lastname: string, firstname: string, email: string, phone: string) {
    this.lastname = lastname;
    this.firstname = firstname;
    this.email = email;
    this.phone = phone;
  }
}

export class EditProfil {
  id: number;
  profil: string;

  constructor(id: number, profil: string) {
    this.id = id;
    this.profil = profil;
  }
}

export class Person {
  id: number;
  lastname: string;
  firstname: string;
  email: string;
  phone: string;
  profil: string;
  date_in: string;
  date_edit: Date;
  manager: Person;
  clientLocation: string;
  location: string;
}

export class PersonVM {
  id: number;
  lastName: string;
  firstName: string;
  email: string;
  phone: string;
  profil: string;
  dateIn: string;
  dateEdit: Date;
  clientLocation: string;
  location: string;

  constructor(person: Person, profil: string) {
    this.lastName = person.lastname;
    this.firstName = person.firstname;
    this.lastName = person.lastname;
    this.email = person.email;
    this.phone = person.phone;
    this.profil = profil;
    this.dateIn = person.date_in;
    this.dateEdit = person.date_edit;
    this.id = person.id;
    this.clientLocation = person.clientLocation;
    this.location =  person.location;
  }
}
