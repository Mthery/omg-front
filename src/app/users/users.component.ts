import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {ApolloQueryResult} from 'apollo-client';

import {QueryService} from 'src/app/apollo/query.service';
import {Profil} from 'src/app/constants/profil';
import {PersonsData} from 'src/app/models/datas';
import {Person, PersonVM} from 'src/app/models/person';
import {AuthorizationService} from "../welcome-page/authorization.service";
import {BottomSheetComponent} from "../alerts/bottom-sheet/bottom-sheet.component";
import BottomSheetResponse from "../models/bottomSheetResponse";
import {Section} from "../models/field-base";
import {MatBottomSheet} from "@angular/material/bottom-sheet";
import {MutationService} from "../apollo/mutation.service";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  @Input() data: PersonVM[];

  constructor(private queryService: QueryService,
              public authorizationService: AuthorizationService,
              private bottomSheet: MatBottomSheet,
              private mutationService: MutationService) {
  }

  showAddupdate: boolean = false;
  personId: number;
  displayedColumns: string[] = ['lastName',
    'firstName',
    'email',
    'phone',
    'profil',
    'arrivee',
    'cv',
    'updatedelete'];
  dataSource: MatTableDataSource<PersonVM>;

  @ViewChild(MatSort) sort: MatSort;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit(): void {
    this.queryPersons();
  }

  onClose(): void {
    this.showAddupdate = false;
    this.personId = null;
  }

  onClickAdd(): void {
    this.personId = null;
    this.showAddupdate = true;
  }

  /**
   * @param id Id
   */
  onEdit(id: number): void {
    this.personId = id;
    this.showAddupdate = true;
  }

  onDelete(id: number): void {
    const alert = this.bottomSheet.open(BottomSheetComponent, {data: 'Supprimer cet utilisateur ?'});
    alert.afterDismissed()
      .subscribe((data: BottomSheetResponse) => {
        if (data && data.message === "Ok") {
          const personSection: Section = require('src/assets/sections/person');
          const argumentFields = [{key: 'id', type: 'Int', value: id}];

          this.mutationService.mutateSingleRow(personSection.delete, null, argumentFields)
            .then(() => {
              this.queryPersons();
              if (this.personId === id)
                this.showAddupdate = false;
            });
        }
      });
  }

  queryPersons(): void {
    const personSection = require('src/assets/sections/person');

    this.queryService.queryManyRows(personSection.search, personSection.fields)
      .then((result: ApolloQueryResult<PersonsData<Person[]>>) => {
        this.dataSource = new MatTableDataSource<PersonVM>(result.data.persons.map(m => new PersonVM(m, Profil[m.profil])));
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
  }
}
