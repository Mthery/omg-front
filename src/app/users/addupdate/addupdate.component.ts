import {Component, EventEmitter, Input, OnChanges, Output, ViewChild} from '@angular/core';
import BottomSheetResponse from '../../models/bottomSheetResponse';
import {MatBottomSheet} from '@angular/material/bottom-sheet';
import {Person, PersonVM} from '../../models/person';
import {Section} from '../../models/field-base';
import {MutationService} from '../../apollo/mutation.service';
import {MatSnackBar} from "@angular/material/snack-bar";
import {ApolloQueryResult} from "apollo-client";
import {PersonData, PersonsData} from "../../models/datas";
import {QueryService} from "../../apollo/query.service";
import {AuthService} from "../../welcome-page/auth.service";
import {Profil} from "../../constants/profil";
import {BottomSheetComponent} from "../../alerts/bottom-sheet/bottom-sheet.component";
import {Moment} from "moment";
import {first} from 'rxjs/operators';

@Component({
  selector: 'app-addupdate',
  templateUrl: './addupdate.component.html',
  styleUrls: ['./addupdate.component.css']
})
export class AddupdateComponent implements OnChanges {

  @Input() personId?: number;
  @Output() added: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() close: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('form') form: HTMLFormElement;

  persons: PersonVM[] = [];
  person: Person = {} as Person;
  title: string;

  userId: number;
  firstname: string;
  lastname: string;
  phone: number;
  email: string;
  profils = Profil;
  date_in: Date;

  managers: Person[] = [];
  selectedManagerId: number;

  private personSection: Section;

  constructor(private mutationService: MutationService,
              private bottomSheet: MatBottomSheet,
              private snackBar: MatSnackBar,
              private queryService: QueryService,
              private authService: AuthService) {
    this.personSection = require('src/assets/sections/person');
  }

  emitClose(): void {
    if (!this.form.submitted && this.form.touched) {
      const alert = this.bottomSheet.open(BottomSheetComponent, {data: 'Êtes-vous certain(e) de vouloir quitter ?'});
      alert.afterDismissed()
        .subscribe((data: BottomSheetResponse) => {
          if (data && data.message === "Ok") {
            this.close.emit(true);
          }
        });
    } else {
      this.close.emit(true);
    }
  }

  ngOnChanges(): void {
    this.setTitle();
    this.queryService.queryManyRows(this.personSection.search, this.personSection.fields)
      .then((result: ApolloQueryResult<PersonsData<Person[]>>) => {
        this.resetFields();
        this.person = {} as Person;

        this.managers = result.data.persons.filter(
          p => p.profil === "MANAGER"
        );

        if (this.personId) {
          this.queryService.querySingleRow(this.personSection.detail, this.personSection.fields, this.personId.toString())
            .then((result: ApolloQueryResult<PersonData<Person>>) => {
              this.setFields(result.data.person);
            });
        }
      });
  }

  private resetFields(): void {
    this.persons = [];
    this.lastname = null;
    this.firstname = null;
    this.email = null;
    this.phone = null;
    this.userId = null;
    this.date_in = null;
  }

  /**
   * Permet de remplir les champs du formulaire avec le collaborateur à modifier/visualiser
   * @param person Le collaborateur à modifier/visualiser
   */
  private setFields(person: Person): void {
    this.date_in = person.date_in ? new Date(person.date_in) : null;
    this.selectedManagerId = person.manager ? person.manager.id : null;

    this.person.firstname = person.firstname;
    this.person.lastname = person.lastname;
    this.person.email = person.email;
    this.person.phone = person.phone;
    this.person.profil = person.profil;
    this.person.date_in = person.date_in;
  }

  private sendRequest(): void {
    var promise;

    var p: any = this.person;
    if (this.selectedManagerId) {
      p = {
        ...this.person,
        collaborator: {
          manager_id: this.selectedManagerId
        }
      };
    }

    if (!this.personId) {
      this.userId = null;

      const argumentFields = [{
        key: this.personSection.create,
        type: this.personSection.create,
        value: p
      }];
      promise = this.mutationService.mutateSingleRow(this.personSection.create, null, argumentFields);
    } else {
      const argumentFields = [{
        key: this.personSection.edit,
        type: this.personSection.edit,
        value: p
      },
        {key: 'id', type: 'Int', value: this.personId}];
      promise = this.mutationService.mutateSingleRow(this.personSection.edit, null, argumentFields);
    }

    promise
      .then(() => {
        this.snackBar.open('Utilisateur créé.', 'Fermer', {duration: 2000, panelClass: 'success'});
        this.added.emit(true);
      })
      .catch(() => {
        this.snackBar.open('Erreur dans le formulaire.', 'Fermer', {
          duration: 2000,
          panelClass: 'danger'
        });
      });
  }

  onSubmit(): void {
    this.authService.userIdSubject
      .pipe(first())
      .subscribe(userId => {
        this.userId = userId;
        this.sendRequest();
      });

    this.authService.emitUserId();
  }

  setDate(val: Moment, debut: boolean): void {
    if (val) {
      if (debut)
        this.person.date_in = val.format('YYYY-MM-DD');
    }
  }

  private setTitle(): void {
      this.title = this.personId ? 'Modifier l\'utilisateur' : 'Ajouter un utilisateur';
  }

}
