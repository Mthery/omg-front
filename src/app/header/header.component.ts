import { Component, OnInit } from '@angular/core';

import { Profil } from 'src/app/constants/profil';
import { AuthService } from 'src/app/welcome-page/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isAdmin: boolean;
  isAuth: boolean;

  constructor(public authService: AuthService) {}

  ngOnInit(): void {
    this.authService.isAuthSubject
      .subscribe(isAuth => {
        this.isAuth = isAuth;
      });

    this.authService.roleSubject
      .subscribe(role => {
        this.isAdmin = (Profil[role] === Profil.IC || Profil[role] === Profil.MANAGER);
      });

    this.authService.emitIsAuth();
    this.authService.emitRole();
  }
}
