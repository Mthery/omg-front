import { Component, EventEmitter, Input, OnChanges, OnInit, Output, ViewChild } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ApolloQueryResult } from 'apollo-client';
import { first } from 'rxjs/operators';

import { BottomSheetComponent } from 'src/app/alerts/bottom-sheet/bottom-sheet.component';
import { MutationService } from 'src/app/apollo/mutation.service';
import BottomSheetResponse from 'src/app/models/bottomSheetResponse';
import { DuplicateMissionData } from 'src/app/models/datas';
import { Section } from 'src/app/models/field-base';
import { MissionVM } from 'src/app/models/mission';
import { AuthService } from 'src/app/welcome-page/auth.service';
import { AuthorizationService } from 'src/app/welcome-page/authorization.service';

@Component({
  selector: 'app-missions-array',
  templateUrl: './missions-array.component.html',
  styleUrls: ['./missions-array.component.css']
})
export class MissionsArrayComponent implements OnChanges, OnInit {
  @Input() data: MissionVM[];
  @Input() missionId?: number;
  @Input() terminees: boolean;
  @Output() deleted: EventEmitter<number> = new EventEmitter<number>();
  @Output() edited: EventEmitter<number> = new EventEmitter<number>();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  dataSource: MatTableDataSource<MissionVM>;
  displayedColumns: string[] = ['priority',
    'agency',
    'createdBy',
    'client',
    'clientLocation',
    'description',
    'startDate',
    'stopDate',
    'status',
    'type',
    'candidates',
    'delete'];
  editTooltip: string;

  constructor(private authService: AuthService,
    public authorizationService: AuthorizationService,
    private bottomSheet: MatBottomSheet,
    private mutationService: MutationService,
    private snackBar: MatSnackBar) {}

  ngOnInit(): void {
    if (this.authorizationService.canDo('editMission') && !this.terminees)
      this.editTooltip = 'Modifier l\'offre';
    else
      this.editTooltip = 'Voir le détail de l\'offre';
  }

  ngOnChanges(): void {
    setTimeout(() => {
      this.dataSource = new MatTableDataSource<MissionVM>(this.data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  /**
   * Demande à l'utilisateur une confirmation, puis delete l'offre sélectionnée
   * Emit ensuite l'id de l'offre supprimée vers le parent (MissionsComponent) pour lui dire d'update la liste des offres
   */
  onDelete(id: number): void {
    const alert = this.bottomSheet.open(BottomSheetComponent, { data: 'Êtes-vous certain(e) de vouloir supprimer cette offre ?' });
    alert.afterDismissed()
      .subscribe((data: BottomSheetResponse) => {
        if (data && data.message === "Ok") {
          const missionSection: Section = require('src/assets/sections/mission');
          const argumentFields = [{ key: 'id', type: 'Int', value: id }];

          this.mutationService.mutateSingleRow(missionSection.delete, null, argumentFields)
            .then(() => {
              this.snackBar.open('Offre supprimée', 'Fermer', { duration: 2000, panelClass: 'success' });
              this.deleted.emit(id);
            })
            .catch(() => {
              this.snackBar.open('Erreur lors de la suppression de l\'offre', 'Fermer', { duration: 2000, panelClass: 'danger' });
            });
        }
      });
  }

  /**
   * Va chercher l'id de l'utilisateur, puis duplique l'offre
   * @param id Id de la mission à dupliquer
   */
  onDuplicate(id: number): void {
    this.authService.userIdSubject
      .pipe(first())
      .subscribe(userId => this.duplicateMission(id, userId));
    this.authService.emitUserId();
  }

  /**
   * Emit l'id d'une offre vers le parent (MissionsComponent) pour lui dire que l'on veut editer cette offre
   */
  onEdit(id: number): void {
    this.edited.emit(id);
  }

  /**
   * Duplique la mission sélectionnée puis demande au composant parent de reload la liste des mission
   * @param id Id de la mission à dupliquer
   * @param userId Id de l'utilisateur connecté (qui duplique l'offre donc)
   */
  private duplicateMission(id: number, userId: number): void {
    const argumentFieldId = { key: 'id', type: 'Int', value: id };
    const argumentFieldCreatedBy = { key: 'createdBy', type: 'Int', value: userId };
    this.mutationService.mutateSingleRow('duplicateMission', null, [argumentFieldId, argumentFieldCreatedBy])
      .then((result: ApolloQueryResult<DuplicateMissionData>) => {
        this.snackBar.open('Offre dupliquée', 'Fermer', { duration: 2000, panelClass: 'success' });
        this.deleted.emit(result.data.duplicateMission);
      })
      .catch(() => {
        this.snackBar.open('Erreur lors de la duplication de l\'offre', 'Fermer', { duration: 2000, panelClass: 'danger' });
      });
  }
}
