import { KeyValue } from '@angular/common';
import { Component, EventEmitter, Input, OnChanges, Output, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ApolloQueryResult } from 'apollo-client';
import { Moment } from 'moment';
import * as moment from 'moment';
import { first } from 'rxjs/operators';

import { BottomSheetComponent } from 'src/app/alerts/bottom-sheet/bottom-sheet.component';
import { CandidateAlertComponent } from 'src/app/alerts/candidate-alert/candidate-alert.component';
import { ClientAlertComponent } from 'src/app/alerts/client-alert/client-alert.component';
import { SkillAlertComponent } from 'src/app/alerts/skill-alert/skill-alert.component';
import { QueryService } from 'src/app/apollo/query.service';
import { MutationService } from 'src/app/apollo/mutation.service';
import { Agencies } from 'src/app/constants/agencies';
import { Priorities } from 'src/app/constants/priorities';
import { Status } from 'src/app/constants/status';
import { Types } from 'src/app/constants/types';
import { SkillAlertData, CandidateAlertData, ClientAlertData } from 'src/app/models/alert';
import BottomSheetResponse from 'src/app/models/bottomSheetResponse';
import { ClientRequest, ClientVM } from 'src/app/models/client';
import { CollaboratorsData, CreateMissionData, LocationsData, MissionData, MissionSkillsData } from 'src/app/models/datas';
import { Section } from 'src/app/models/field-base';
import { Location } from 'src/app/models/location';
import { Mission, MissionCandidate, MissionRequest } from 'src/app/models/mission';
import { MissionSkillRequest, MissionSkill, MissionSkillSuggestion } from 'src/app/models/missionSkill';
import { Person } from 'src/app/models/person';
import { AuthService } from 'src/app/welcome-page/auth.service';
import { AuthorizationService } from 'src/app/welcome-page/authorization.service';

@Component({
  selector: 'app-add-or-update',
  templateUrl: './add-or-update.component.html',
  styleUrls: ['./add-or-update.component.css']
})
export class AddOrUpdateComponent implements OnChanges {
  @Input() missionId?: number;
  @Input() terminees: boolean;
  @Output() added: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() close: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('form') form: NgForm;

  agencies = Agencies;
  candidates: Person[] = [];
  clients: ClientVM[] = [];
  collaborators: Person[];
  dateDebut: Date;
  dateFin: Date;
  mission: MissionRequest = {} as MissionRequest;
  missionSkills: MissionSkillRequest[] = [];
  priorities = Priorities;
  selectedClient: ClientVM;
  selectedClientId: string;
  statusEnum = Status;
  title: string;
  types = Types;
  userId: number;

  private candidateSection: Section;
  private collaboratorSection: Section;
  private locationSection: Section;
  private missionSection: Section;
  private missionSkillSection: Section;

  constructor(private authService: AuthService,
    private authorizationService: AuthorizationService,
    private bottomSheet: MatBottomSheet,
    private dialog: MatDialog,
    private mutationService: MutationService,
    private queryService: QueryService,
    private snackBar: MatSnackBar) {
      this.candidateSection = require('src/assets/sections/candidate');
      this.collaboratorSection = require('src/assets/sections/collaborator');
      this.locationSection = require('src/assets/sections/location');
      this.missionSection = require('src/assets/sections/mission');
      this.missionSkillSection = require('src/assets/sections/missionSkill');
    }

  ngOnChanges(): void {
    this.setTitle();

    this.getData(true);

    this.queryService.queryManyRows(this.collaboratorSection.search, this.collaboratorSection.fields.filter(f => ["id", "lastname", "firstname"].indexOf(f.key) >= 0))
      .then((result: ApolloQueryResult<CollaboratorsData<Person[]>>) => {
        this.collaborators = result.data.collaborators;
        this.collaborators.sort(this.sortPersons);
      });
  }

  /**
   * Méthode permettant de savoir si l'utilisateur a le droit d'utiliser le form
   */
  canChangeForm(): boolean {
    const canChangeForm = this.missionId ? this.authorizationService.canDo('editMission') : this.authorizationService.canDo('createMission');
    return canChangeForm && !this.terminees;
  }

  /**
   * Si le formulaire a été modifié mais pas envoyé, demande confirmation à l'utilisateur avant de le fermer
   * Sinon ferme le formulaire
   */
  emitClose(): void {
    if (!this.form.submitted && this.form.touched && this.canChangeForm()) {
      const alert = this.bottomSheet.open(BottomSheetComponent, { data: 'Êtes-vous certain(e) de vouloir fermer le formulaire sans avoir sauvegardé ?' });
      alert.afterDismissed()
        .subscribe((data: BottomSheetResponse) => {
          if (data && data.message === "Ok") {
            this.close.emit(true);
          }
        });
    } else {
      this.close.emit(true);
    }
  }

  /**
   * Ouverture de la pop-up pour créer/update un client
   * @param client Client à modifier (null si on veut créer un nouveau)
   */
  onAddClient(client: ClientVM): void {
    const data = new ClientAlertData(client ? client : {location: []} as ClientVM, []);

    const dialogRef = this.dialog.open(ClientAlertComponent, {
      data,
      disableClose: true,
      width: '30vw'
    });

    dialogRef.afterClosed().subscribe((result: ClientAlertData) => {
      if (result) {
        if (result.client.label && result.client.location.length > 0) {
          const clientRequest = new ClientRequest(result.client);
          clientRequest.location = clientRequest.location.map(l => {return {label: l.label} as Location});
          clientRequest.location = clientRequest.location.filter(l => l.label);
          
          // cas de la création d'un nouveau client
          if (!client) {
            const argumentFields = [{ key: 'createClientWithLocation', type: '[createClientWithLocation!]', value: [clientRequest] }];
            this.mutationService.mutateSingleRow('createClientsWithLocations', null, argumentFields)
              .then(() => {
                this.snackBar.open('Client créé.', 'Fermer', { duration: 2000, panelClass: 'success' });
                this.getData(false);
              })
              .catch(() => {
                this.snackBar.open('Echec lors de la création du client.', 'Fermer', { duration: 2000, panelClass: 'danger' });
              });
          }
          // cas de l'update d'un client existant 
          else {
            clientRequest.id = client.id;
            const argumentFields = [{ key: 'updateClientWithLocation', type: 'updateClientWithLocation', value: clientRequest }];
            this.mutationService.mutateSingleRow('updateClientWithLocation', null, argumentFields)
              .then(() => {
                this.snackBar.open('Client mis à jour.', 'Fermer', { duration: 2000, panelClass: 'success' });
                this.getData(false);
              })
              .catch(() => {
                this.snackBar.open('Echec lors de la mise à jour du client.', 'Fermer', { duration: 2000, panelClass: 'danger' });
              });
          }
        }
      }
    });
  }

  /**
   * Ouverture de la pop-up pour gérer les candidatures liées à l'offre
   */
  onClickCandidates(): void {
    const data = new CandidateAlertData(this.candidates, this.missionSkills.map(ms => new MissionSkillSuggestion(ms)), !this.canChangeForm());

    const dialogRef = this.dialog.open(CandidateAlertComponent, {
      data: JSON.parse(JSON.stringify(data)),
      disableClose: true,
      width: '30vw'
    });

    dialogRef.afterClosed().subscribe((result: CandidateAlertData) => {
      if (result) {
        this.candidates = result.candidates;
        this.form.form.markAsTouched();
        
        if (this.candidates.length > 0 && this.mission.status === Object.keys(Status).find(k => Status[k] === Status.OPEN)) {
          console.log('ah');
          this.mission.status = Object.keys(Status).find(k => Status[k] === Status.SENT);
        }
      }
    });
  }

  /**
   * Ouverture de la pop-up pour gérer les skills liés à l'offre
   */
  onClickSkills(): void {
    const data = new SkillAlertData(this.missionSkills, !this.canChangeForm());

    const dialogRef = this.dialog.open(SkillAlertComponent, {
      data: JSON.parse(JSON.stringify(data)),
      disableClose: true,
      width: '45vw'
    });

    dialogRef.afterClosed().subscribe((result: SkillAlertData) => {
      if (result && result.missionSkills && !result.readOnly) {
        this.missionSkills = result.missionSkills;
        this.form.form.markAsTouched();
      }

      this.missionSkills = this.missionSkills.filter(ms => ms.level && ms.mandatory !== null && ms.mandatory !== undefined && ms.skill_id);
    });
  }

  /**
   * Vient chercher l'id de l'utilisateur connecté, puis envoie la requête d'ajout ou d'update
   */
  onSubmit(): void {
    this.authService.userIdSubject
      .pipe(first())
      .subscribe(userId => {
        this.userId = userId;

        this.sendRequest();
      });
      
    this.authService.emitUserId();
  }

  /**
   * Vient set this.selectedClient, et si ce client n'a qu'une location, la sélectionne aussi
   */
  onSelectClient(): void {
    this.selectedClient = this.clients.find(c => c.id.toString() === this.selectedClientId);

    if (this.selectedClient.location.length === 1)
      this.mission.client_location_id = this.selectedClient.location[0].id;
  }

  /**
   * Vient setter la date de début ou de fin de l'offre, correctement formatée
   * Vient ensuite vérifier si la date de début est bien < à la date de fin
   * @param val Date entrée par l'utilisateur
   * @param debut True pour la date de début, false pour la date de fin
   */
  setDate(val: Moment, debut: boolean): void {
    if (val) {
      if (debut)
        this.mission.date_start = val.format('YYYY-MM-DD');
      else
        this.mission.date_out = val.format('YYYY-MM-DD');

      if (this.mission.date_start && this.mission.date_out) {
        if (moment(this.dateDebut, 'YYYY-MM-DD').isAfter(moment(this.dateFin, 'YYYY-MM-DD'))) {
          this.form.form.controls['fin'].setErrors({'incorrect': true});
        } else {
          this.form.form.controls['fin'].setErrors(null);
        }
      } else {
        this.form.form.controls['fin'].setErrors(null);
      }
    }
  }

  /**
   * Permet de trier une énumération par ordre alphabétique (par valeur, et non par clé)
   */
  sortEnum(a: KeyValue<number,string>, b: KeyValue<number,string>): number {
    return a.value.localeCompare(b.value);
  }

  /**
   * Notifie l'utilisateur du succés de l'opération, remet certaines propriétés du form à 0 puis notifie le parent pour refresh la liste
   */
  private afterSucessRequest(): void {
    this.snackBar.open('Offre enregistrée.', 'Fermer', { duration: 2000, panelClass: 'success' });
    this.form.resetForm(this.form.value);
    this.added.emit(true);
  }

  /**
   * Récupère les clients et locations en bdd, puis les formate correctement
   * Vient ensuite récupérer les info de la mission si on est en mode edit
   * @param getMissionData True si on doit récup les infos de la mission, false sinon
   */
  private getData(getMissionData: boolean): void {
    this.queryService.queryManyRows(this.locationSection.search, this.locationSection.fields)
      .then((result: ApolloQueryResult<LocationsData<Location[]>>) => {
        if (getMissionData)
          this.resetFields();
        else
          this.clients.forEach(c => c.location = []);

        result.data.locations.forEach(loc => {
          const client: ClientVM = this.clients.find(c => c.id === loc.client.id && c.label === loc.client.label);

          if (client)
            this.clients.find(c => c.id === loc.client.id && c.label === loc.client.label).location.push(loc);
          else
            this.clients.push(new ClientVM(loc.client.id, loc.client.label, loc));
        });

        this.sortClients();

        if (this.missionId && getMissionData) {
          this.queryService.querySingleRow(this.missionSection.detail, this.missionSection.fields, this.missionId.toString())
            .then((result: ApolloQueryResult<MissionData<Mission>>) => {
              this.setFields(result.data.mission);
            });
    
          const argumentFields = { key: 'missionId', type: 'Int', value: this.missionId };
          this.queryService.queryManyRowsWithArgs(this.missionSkillSection.search, this.missionSkillSection.fields, [argumentFields])
            .then((result: ApolloQueryResult<MissionSkillsData<MissionSkill[]>>) => {
              if (result.data.missionSkills.length > 0)
                this.missionSkills = result.data.missionSkills.map(ms => new MissionSkillRequest(ms));
            });
        }
      });
  }

  /**
   * Permet de remettre à 0 les champs du formulaire
   */
  private resetFields(): void {
    this.clients = [];
    this.dateDebut = null;
    this.dateFin = null;
    this.mission = {} as MissionRequest;
    this.missionSkills = []
    this.selectedClient = null;
    this.selectedClientId = null;
    this.userId = null;
    this.form.resetForm();
  }

  /**
   * Envoie la requête d'ajout ou d'update
   * En cas d'ajout, renseigne l'utilisateur connecté comme créateur de l'offre
   */
  private sendRequest(): void {
    var promise;

    if (!this.missionId && !this.userId)
      return;

    if (!this.missionId) {
      this.mission.created_by = this.userId;
      const argumentFieldsCreateMission = [{ key: this.missionSection.create, type: this.missionSection.create, value: this.mission }];
      promise = this.mutationService.mutateSingleRow(this.missionSection.create, null, argumentFieldsCreateMission);
    } else {
      const argumentFieldsEditMission = [{ key: this.missionSection.edit, type: this.missionSection.edit, value: this.mission },
        { key: 'id', type: 'Int', value: this.missionId }];
      promise = this.mutationService.mutateSingleRow(this.missionSection.edit, null, argumentFieldsEditMission);
    }

    promise
      .then((result: ApolloQueryResult<CreateMissionData>) => {
        this.missionSkills = this.missionSkills.filter(ms => ms.level && ms.mandatory !== null && ms.mandatory !== undefined && ms.skill_id);
        const id = this.missionId ? this.missionId : result.data.createMission;

        // On delete les skills liés à l'offre
        const argumentFieldsMissionId = [{ key: 'missionId', type: 'Int', value: id }];
        this.mutationService.mutateSingleRow(this.missionSkillSection.delete, null, argumentFieldsMissionId)
          .then(() => {
            // Si on a des skills a save, on les save en bdd
            if (this.missionSkills.length > 0) {
              this.missionSkills.forEach(ms => ms.mission_id = id);

              const argumentFieldsCreateMissionSkill = [{ key: this.missionSkillSection.create, type: `[${this.missionSkillSection.create}!]`, value: this.missionSkills }];
              this.mutationService.mutateSingleRow(this.missionSkillSection.create, null, argumentFieldsCreateMissionSkill)
                .then(() => {
                  this.afterSucessRequest();
                })
                .catch(() => {
                  this.snackBar.open('Echec lors de l\'enregistrement de l\'offre.', 'Fermer', { duration: 2000, panelClass: 'danger' });
                });
            } else {
              this.afterSucessRequest();
            }
          })
          .catch(() => {
            this.snackBar.open('Echec lors de l\'enregistrement de l\'offre.', 'Fermer', { duration: 2000, panelClass: 'danger' });
          });

        // On delete les candidats liés à l'offre
        this.mutationService.mutateSingleRow(this.candidateSection.delete, null, argumentFieldsMissionId)
          .then(() => {
            // Si on a des candidats à save, on les save en bdd
            if (this.candidates.length > 0) {
              const missionCandidates: MissionCandidate[] = [];
              this.candidates.forEach(c => missionCandidates.push(new MissionCandidate(c.id, id)));

              const argumentFieldsCreateMissionCandidate = [{ key: this.candidateSection.create, type: `[${this.candidateSection.create}!]`, value: missionCandidates }];
              this.mutationService.mutateSingleRow(this.candidateSection.create, null, argumentFieldsCreateMissionCandidate)
                .then(() => {
                  this.afterSucessRequest();
                })
                .catch(() => {
                  this.snackBar.open('Echec lors de l\'enregistrement de l\'offre.', 'Fermer', { duration: 2000, panelClass: 'danger' });
                });
            } else {
              this.afterSucessRequest();
            }
          })
          .catch(() => {
            this.snackBar.open('Echec lors de l\'enregistrement de l\'offre.', 'Fermer', { duration: 2000, panelClass: 'danger' });
          });
      })
      .catch(() => {
        this.snackBar.open('Echec lors de l\'enregistrement de l\'offre.', 'Fermer', { duration: 2000, panelClass: 'danger' });
      });
  }

  /**
   * Permet de remplir les champs du formulaire avec l'offre à modifier/visualiser
   * @param mission L'offre à modifier/visualiser
   */
  private setFields(mission: Mission): void {
    this.selectedClientId = mission.client.id.toString();
    this.selectedClient = this.clients.find(c => c.id.toString() === this.selectedClientId);
    this.dateDebut = mission.date_start ? new Date(mission.date_start) : null;
    this.dateFin = mission.date_out ? new Date(mission.date_out) : null;
    this.candidates = mission.candidates;

    this.mission.client_location_id = mission.client_location.id;
    this.mission.priority = mission.priority;
    this.mission.type = mission.type;
    this.mission.status = mission.status;
    this.mission.adr = mission.adr;
    this.mission.description = mission.description;
    this.mission.date_start = mission.date_start;
    this.mission.date_out = mission.date_out;
    this.mission.agency = mission.agency;
    if (mission.replaced)
      this.mission.replaced = mission.replaced.id;
    this.mission.contact_email = mission.contact_email;
    this.mission.contact_phone = mission.contact_phone;
  }

  /**
   * Change le titre du formulaire selon les droits de l'utilisateur et l'action souhaitée
   */
  private setTitle(): void {
    if (!this.canChangeForm())
      this.title = 'Détail de l\'offre';
    else {
      this.title = this.missionId ? 'Modifier l\'offre' : 'Ajouter une offre';
    }
  }

  /**
   * Trie les clients et leurs locations par label
   */
  private sortClients(): void {
    this.clients = this.clients.sort((a, b) => a.label.toUpperCase() > b.label.toUpperCase() ? 1 : a.label.toUpperCase() === b.label.toUpperCase() ? 0 : -1);
    this.clients.forEach(c => {
      c.location = c.location.sort((a, b) => a.label.toUpperCase() > b.label.toUpperCase() ? 1 : a.label.toUpperCase() === b.label.toUpperCase() ? 0 : -1);
    });
  }

  /**
   * Méthode à passer à .sort pour trier un tableau de Person par nom, par ordre alphabétique
   */
   private sortPersons(p1: Person, p2: Person): number {
    if (p1.lastname.toUpperCase() > p2.lastname.toUpperCase())
      return 1;
    
    if (p1.lastname.toUpperCase() < p2.lastname.toUpperCase())
      return -1;

    return 0;
  }
}
