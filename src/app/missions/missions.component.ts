import { Component, OnInit } from '@angular/core';
import { MatDatepicker } from '@angular/material/datepicker';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { ApolloQueryResult } from 'apollo-client';
import { Moment } from 'moment';
import * as moment from 'moment';
import { first } from 'rxjs/operators';

import { ImportAlertComponent } from 'src/app/alerts/import-alert/import-alert.component';
import { QueryService } from 'src/app/apollo/query.service';
import { Profil } from 'src/app/constants/profil';
import { Status } from 'src/app/constants/status';
import { Types } from 'src/app/constants/types';
import { MissionsData, PersonData } from 'src/app/models/datas';
import { Section } from 'src/app/models/field-base';
import { Mission, MissionVM } from 'src/app/models/mission';
import { Person } from 'src/app/models/person';
import { AuthService } from 'src/app/welcome-page/auth.service';
import { AuthorizationService } from 'src/app/welcome-page/authorization.service';

@Component({
  selector: 'app-missions',
  templateUrl: './missions.component.html',
  styleUrls: ['./missions.component.css']
})
export class MissionsComponent implements OnInit {
  // Tableaux contenants les valeurs de filtres
  agencies: string[] = [];
  clients: string[] = [];
  collaborators: string[] = [];
  datesDebut: string[] = [];
  datesFin: string[] = [];
  demandeurs: string[] = [];
  descriptions: string[] = [];
  priorities: string[] = [];
  status: string[] = [];
  types: string[] = [];

  // Tableaux contenant les filtres sélectionnés par l'utilisateur
  agencyFilter: string = '';
  clientFilter: string = '';
  collaboratorFilter: string = '';
  dateDebutFilter: string = '';
  dateFinFilter: string = '';
  demandeurFilter: string = '';
  descriptionFilter: string = '';
  priorityFilter: string = '';
  statusFilter: string = '';
  typeFilter: string = '';

  // Tableaux contenants les offres
  filteredMissions: MissionVM[] = [];
  missions: MissionVM[] = [];

  // Variables pour afficher/masquer de l'html
  showAddOrUpdate: boolean = false;
  showFilters: boolean = false;

  dateDebut: Date;
  dateFin: Date;
  filterByDates: boolean = false;
  missionId: number;
  terminees: boolean = false;

  private isInit: boolean = true;
  private missionSection: Section;
  private personSection: Section;

  constructor(private authService: AuthService,
    public authorizationService: AuthorizationService,
    private dialog: MatDialog,
    private queryService: QueryService,
    private route: ActivatedRoute) {
      this.missionSection = require('src/assets/sections/mission');
      this.personSection = require('src/assets/sections/person');
    }

  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
        this.terminees = !!params.terminees;
        this.resetAll();
        this.queryMissions();
      });
  }

  /**
   * Filtre le tableau filteredMissions selon les divers filtres
   */
  filter(): void {
    this.filteredMissions = this.missions.filter(mission => {
      return (this.agencyFilter === '' || mission.agency === this.agencyFilter) &&
        (this.clientFilter === '' || mission.client === this.clientFilter) &&
        (this.collaboratorFilter === '' || mission.candidates.indexOf(this.collaboratorFilter) >= 0) &&
        (this.dateDebutFilter === '' || mission.startDate === this.dateDebutFilter) &&
        (this.dateFinFilter === '' || mission.stopDate === this.dateFinFilter) &&
        (this.demandeurFilter === '' || mission.createdBy === this.demandeurFilter) &&
        (this.descriptionFilter === '' || mission.description === this.descriptionFilter) &&
        (this.priorityFilter === '' || mission.priority === this.priorityFilter) &&
        (this.statusFilter === '' || mission.status === this.statusFilter) &&
        (this.typeFilter === '' || mission.type === this.typeFilter)
    });
    this.filterBetweenDates();
  }

  /**
   * Affiche le formulaire, et reset missionId pour que le formulaire se mette en mode ajout et non update
   */
  onClickAdd(): void {
    this.missionId = null;
    this.showAddOrUpdate = true;
  }

  /**
   * Reset les tableaux contenants les valeurs de filtres, puis les remplis et les trie
   */
  onClickFilter(): void {
    this.resetArrays();

    if (!this.showFilters)
      this.getDistinct();

    this.showFilters = !this.showFilters;
    this.filter();
  }

  /**
   * Affiche l'alert permettant d'importer des offres via csv
   */
  onClickImport(): void {
    const dialog = this.dialog.open(ImportAlertComponent, {
      disableClose: true,
      width: '45vw'
    });

    dialog.afterClosed()
      .subscribe(() => {
        this.queryMissions();
      });
  }

  /**
   * Ferme le formulaire et remet à null l'id d'offre à update
   */
  onClose(): void {
    this.showAddOrUpdate = false;
    this.missionId = null;
  }

  /**
   * Update la liste des offres après une suppression
   * Si l'offre supprimée était celle en cours d'update, on ferme le formulaire pour éviter tout soucis
   * @param id Id de l'offre supprimée
   */
  onDelete(id: number): void {
    this.queryMissions();

    if (id === this.missionId) {
      this.onClose();
    }
  }

  /**
   * Vient setter la variable missionId, et affiche le formulaire s'il ne l'est pas déjà
   * @param id Id de l'offre que l'on update
   */
  onEdit(id: number): void {
    this.missionId = id;
    this.showAddOrUpdate = true;
  }

  /**
   * Vient set le mois et le jour (soit le 1er, soit le dernier jour du mois) pour la data de fin ou de début
   * @param date Date sélectionnée par l'utilisateur
   * @param debut True si date de début, false pour la date de fin
   * @param datepicker Datepicker à fermer (on ne sélectionne pas le jour)
   */
  onSelectMonth(date: Moment, debut: boolean, datepicker: MatDatepicker<Moment>): void {
    if (debut) {
      var d = new Date(this.dateDebut);
      d.setMonth(date.month());
      d.setDate(1);
      this.dateDebut = d;
    }
    else {
      var f = new Date(this.dateFin);
      f.setMonth(date.month() + 1);
      // En mettant 0 comme jour, on se retrouve avec le dernier jour du mois précédent, d'où le month + 1
      f.setDate(0);
      this.dateFin = f;
    }
    
    datepicker.close();
    if (this.filterByDates)
      this.switchFilterByDates(false);
  }

  /**
   * Vient set l'année pour la date de fin ou de début
   * @param date Date sélectionnée par l'utilisateur
   * @param debut True si date de début, false pour la date de fin
   */
  onSelectYear(date: Moment, debut: boolean): void {
    if (debut) {
      this.dateDebut = new Date();
      this.dateDebut.setFullYear(date.year());
    }
    else {
      this.dateFin = new Date();
      this.dateFin.setFullYear(date.year());
    }
  }

  /**
   * Récupère les offres en base, puis les trie par priorité
   * Si la page vient d'être lancée, on filtre les offres sur l'utilisateur (si ingé commercial)
   */
  queryMissions(): void {
    this.queryService.queryManyRows(this.missionSection.search, this.missionSection.fields)
      .then((result: ApolloQueryResult<MissionsData<Mission[]>>) => {
        const statusTermine: string[] = [Status.LOSS, Status.WIN, Status.CLOSED, Status.UNANSWERED, Status.SWITCH];
        const missions = this.terminees ?
          result.data.missions
            .filter(m => statusTermine.indexOf(Status[m.status]) >= 0) :
          result.data.missions
            .filter(m => statusTermine.indexOf(Status[m.status]) < 0);
          
        this.missions = missions.map(m => new MissionVM(m, Status[m.status], Types[m.type], m.priority));
        this.filteredMissions = this.missions;
        this.filter();
        
        if (this.isInit) {
          this.isInit = false;
          this.subscribeRole();
          this.authService.emitRole();
        }
      });
  }

  /**
   * Change la valeur de filterByDates, puis filtre les missions
   * Si les filtres sont affichés, on les cache (sinon on pouvait filtrer sur des données qui ne sont plus dans le tableau)
   * @param filter Valeur à assigner à filterByDates
   */
  switchFilterByDates(filter: boolean): void {
    this.filterByDates = filter;
    this.filter();
    
    if (this.showFilters) {
      this.onClickFilter();
    }
  }

  /**
   * Filtre le tableau filteredMissions par date de début et de fin, de sorte à ce que ces 2 dates soient dans l'interval choisi
   * N'est utilisé que pour les offres terminées
   * @returns
   */
  private filterBetweenDates(): void {
    if (this.dateDebut && this.dateFin && this.filterByDates) {
      this.filteredMissions = this.filteredMissions.filter(m => {
        return ((
            moment(m.startDate).isAfter(moment(this.dateDebut)) && moment(m.startDate).isBefore(moment(this.dateFin))
          ) || !m.startDate)
          &&
          ((
            moment(m.stopDate).isAfter(moment(this.dateDebut)) && moment(m.stopDate).isBefore(moment(this.dateFin))
          ) || !m.stopDate);
      });
    }
  }

  /**
   * On vient récupérer l'id de l'utilisateur, puis l'utilisateur en lui-même
   * Puis on filtre les offres sur le champs demandeur
   */
  private filterUserMissions(): void {
    this.authService.userIdSubject
      .pipe(first())
      .subscribe(id => {
        this.queryService.querySingleRow(this.personSection.detail, 
          this.personSection.fields.filter(f => ["lastname", "firstname"].indexOf(f.key) >= 0),
          id.toString())
          .then((result: ApolloQueryResult<PersonData<Person>>) => {
            this.onClickFilter();
            this.demandeurFilter = `${result.data.person.firstname} ${result.data.person.lastname}`;
            this.filter();
          });
      });

      this.authService.emitUserId();
  }

  /**
   * Récupère les différentes valeurs depuis this.missions pour peupler les tableaux de filtres
   */
  private getDistinct(): void {
    this.filteredMissions.forEach(mission => {
      if (this.agencies.indexOf(mission.agency) < 0 && mission.agency !== null)
        this.agencies.push(mission.agency);

      if (this.clients.indexOf(mission.client) < 0 && mission.client !== null)
        this.clients.push(mission.client);

      mission.candidates.forEach(candidate => {
        if (this.collaborators.indexOf(candidate) < 0 && candidate !== null)
          this.collaborators.push(candidate);
      });

      if (this.datesDebut.indexOf(mission.startDate) < 0 && mission.startDate !== null)
        this.datesDebut.push(mission.startDate);

      if (this.datesFin.indexOf(mission.stopDate) < 0 && mission.stopDate !== null)
        this.datesFin.push(mission.stopDate);

      if (this.demandeurs.indexOf(mission.createdBy) < 0 && mission.createdBy !== null)
        this.demandeurs.push(mission.createdBy);

      if (this.descriptions.indexOf(mission.description) < 0 && mission.description !== null)
        this.descriptions.push(mission.description);

      if (this.priorities.indexOf(mission.priority) < 0 && mission.priority !== null)
        this.priorities.push(mission.priority);

      if (this.status.indexOf(mission.status) < 0 && mission.status !== null)
        this.status.push(mission.status);

      if (this.types.indexOf(mission.type) < 0 && mission.type !== null)
        this.types.push(mission.type);
    });

    this.agencies.sort();
    this.clients.sort();
    this.collaborators.sort();
    this.datesDebut.sort();
    this.datesFin.sort();
    this.demandeurs.sort();
    this.descriptions.sort();
    this.priorities.sort();
    this.status.sort();
    this.types.sort();
  }

  /**
   * Vient reset l'affichage du formulaire, l'id de la mission sélectionnée et les filtres
   */
  private resetAll(): void {
    this.showAddOrUpdate = false;
    this.missionId = null;
    this.showFilters = false;
    this.resetArrays();
  }

  /**
   * Reset les arrays contenants les valeurs de filtres, et les filtres sélectionnés par l'utilisateur
   */
  private resetArrays(): void {
    this.agencies = [];
    this.clients = [];
    this.collaborators = [];
    this.datesDebut = [];
    this.datesFin = [];
    this.demandeurs = [];
    this.descriptions = [];
    this.priorities = [];
    this.status = [];
    this.types = [];

    this.agencyFilter = '';
    this.clientFilter = '';
    this.collaboratorFilter = '';
    this.dateDebutFilter = '';
    this.dateFinFilter = '';
    this.demandeurFilter = '';
    this.descriptionFilter = '';
    this.priorityFilter = '';
    this.statusFilter = '';
    this.typeFilter = '';
  }

  /**
   * Subscribe au sujet contenant le rôle de l'utilisateur
   * Si IC, on filtre les offres sur le champs demandeur
   */
  private subscribeRole(): void {
    this.authService.roleSubject
      .pipe(first())
      .subscribe(role => {
        if (Profil[role] === Profil.IC)
          this.filterUserMissions();
      });
  }
}
