import { Injectable } from '@angular/core';
import { first } from 'rxjs/operators';

import { Authorizations } from 'src/app/constants/authorizations';
import { AuthService } from 'src/app/welcome-page/auth.service';

@Injectable()
export class AuthorizationService {
  private role: string;
  private authorizations = Authorizations;

  constructor(private authService: AuthService) {
  }

  /**
   * Méthode vérifiant si l'utilisateur actuel a le droit de faire l'action passée en paramètre
   * https://media.giphy.com/media/DgLsbUL7SG3kI/giphy.gif
   * @param action Action dont on vérifie les droits
   */
  canDo(action: string): boolean {
    this.authService.roleSubject
      .pipe(first())
      .subscribe(role => {
        this.role = role;
      });

    this.authService.emitRole();

    const authorizedRoles: string = this.authorizations[action];
    if (authorizedRoles) {
      return authorizedRoles.split(',').indexOf(this.role) >= 0;
    }

    return false;
  }
}
