import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import Token from 'src/app/models/token';

@Injectable()
export class AuthService {
  private isAuth: boolean = false;
  private role: string;
  private userId: number;

  constructor(private router: Router) {
    const token = localStorage.getItem('token');

    if (token) {
      this.login(token);
    }
  }

  isAuthSubject: Subject<boolean> = new Subject<boolean>();
  roleSubject: Subject<string> = new Subject<string>();
  userIdSubject: Subject<number> = new Subject<number>();

  /**
   * Met à jour la valeur du subject isAuth
   */
  emitIsAuth(): void {
    this.isAuthSubject.next(this.isAuth);
  }

  /**
   * Met à jour la valeur du subject role
   */
  emitRole(): void {
    this.roleSubject.next(this.role);
  }

  /**
   * Met à jour la valeur du subject userId
   */
  emitUserId(): void {
    this.userIdSubject.next(this.userId);
  }

  login(token: string) {
    const decryptedToken: Token = JSON.parse(atob(token.split('.')[1]));

    // Si la date d'expiration du token est dans le futur
    if (decryptedToken.exp * 1000 > Date.now()) {
      this.isAuth = true;
      this.role = decryptedToken.userRole;
      this.userId = decryptedToken.jti;

      this.emitIsAuth();
      this.emitRole();
    } else {
      this.logout();
    }
  }

  logout() {
    this.isAuth = false;
    this.role = null;
    this.userId = null;

    this.emitIsAuth();
    this.emitRole();

    localStorage.removeItem('token');
    this.router.navigate(['']);
  }
}
