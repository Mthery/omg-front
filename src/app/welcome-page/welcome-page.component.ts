import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from "@angular/router";
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { first } from 'rxjs/operators';

import { AuthService } from 'src/app/welcome-page/auth.service';

@Component({
  selector: 'app-welcome-page',
  templateUrl: './welcome-page.component.html',
  styleUrls: ['./welcome-page.component.css']
})
export class WelcomePageComponent implements OnInit {
  hashage = require('sha.js');
  inputEmail: string;
  inputPassword: string;
  query = gql('query($email: String!, $password: String!) {login(email: $email, password: $password)}');

  constructor(private apollo: Apollo,
    private authService: AuthService,
    private router: Router,
    private snackBar: MatSnackBar) {}

  ngOnInit(): void {
    this.authService.isAuthSubject
      .pipe(first())
      .subscribe(isAuth => {
        if (isAuth)
          this.router.navigate(['users']);
      });

    this.authService.emitIsAuth();
  }

  login(): void {
    this.apollo.query({
      query: this.query,
      variables: {
        email: this.hashage('sha256').update(this.inputEmail).digest('hex'),
        password: this.hashage('sha256').update(this.inputPassword).digest('hex'),
      }
    }).subscribe(
      result => {
        localStorage.setItem('token', result.data['login']);
        this.authService.login(result.data['login']);
        this.router.navigate(['users']);
      },
      error => {
        this.snackBar.open('Mauvaises informations de connexion', 'Fermer', { duration: 2000, panelClass: 'danger' });
      });
  }
}
