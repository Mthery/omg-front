import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import {Observable} from 'rxjs';

import { Profil } from 'src/app/constants/profil';
import {AuthService} from 'src/app/welcome-page/auth.service';

@Injectable({
  providedIn: 'root'
})
export class IsAdminGuard implements CanActivate {
  constructor(private authService: AuthService,
              private router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let isAdmin: boolean;

    this.authService.roleSubject.subscribe(role => {
      isAdmin = (Profil[role] === Profil.IC || Profil[role] === Profil.MANAGER);
    });

    this.authService.emitRole();

    if (!isAdmin)
      this.router.navigate(['']);

    return isAdmin;
  }

}
