import { registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import localeFr from '@angular/common/locales/fr';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FlexModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatMomentDateModule, MomentDateModule } from '@angular/material-moment-adapter';

registerLocaleData(localeFr);

import { AdminComponent } from 'src/app/admin/admin.component';
import { AdminClientsComponent } from 'src/app/admin/admin-clients/admin-clients.component';
import { AdminSkillsComponent } from 'src/app/admin/admin-skills/admin-skills.component';
import { BottomSheetComponent } from 'src/app/alerts/bottom-sheet/bottom-sheet.component';
import { CandidateAlertComponent } from 'src/app/alerts/candidate-alert/candidate-alert.component';
import { ClientAlertComponent } from 'src/app/alerts/client-alert/client-alert.component';
import { CreateSkillAlertComponent } from 'src/app/alerts/create-skill-alert/create-skill-alert.component';
import { ImportAlertComponent } from 'src/app/alerts/import-alert/import-alert.component';
import { RoleAlertComponent } from 'src/app/alerts/role-alert/role-alert.component';
import { SkillAlertComponent } from 'src/app/alerts/skill-alert/skill-alert.component';
import { MutationService } from 'src/app/apollo/mutation.service';
import { QueryService } from 'src/app/apollo/query.service';
import { AppComponent } from 'src/app/app.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { GraphQLModule } from 'src/app/graphql.module';
import { HeaderComponent } from 'src/app/header/header.component';
import { httpInterceptorProviders } from 'src/app/interceptors/index';
import { AddOrUpdateComponent } from 'src/app/missions/add-or-update/add-or-update.component';
import { MissionsArrayComponent } from 'src/app/missions/missions-array/missions-array.component';
import { MissionsComponent } from 'src/app/missions/missions.component';
import { NotFoundComponent } from 'src/app/not-found/not-found.component';
import { ProfilComponent } from 'src/app/profil/profil.component';
import { AddupdateComponent } from "src/app/users/addupdate/addupdate.component";
import { UsersComponent } from 'src/app/users/users.component';
import { AuthService } from 'src/app/welcome-page/auth.service';
import { AuthorizationService } from 'src/app/welcome-page/authorization.service';
import { WelcomePageComponent } from 'src/app/welcome-page/welcome-page.component';

@NgModule({
  declarations: [
    AddupdateComponent,
    AddOrUpdateComponent,
    AdminClientsComponent,
    AdminComponent,
    AdminSkillsComponent,
    AppComponent,
    BottomSheetComponent,
    CandidateAlertComponent,
    ClientAlertComponent,
    CreateSkillAlertComponent,
    HeaderComponent,
    ImportAlertComponent,
    MissionsArrayComponent,
    MissionsComponent,
    NotFoundComponent,
    ProfilComponent,
    RoleAlertComponent,
    SkillAlertComponent,
    UsersComponent,
    WelcomePageComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    FlexModule,
    FormsModule,
    GraphQLModule,
    HttpClientModule,
    MatAutocompleteModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatCardModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTooltipModule,
    MomentDateModule,
    MatMomentDateModule,
    ReactiveFormsModule
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'fr-FR'
    },
    AuthService,
    AuthorizationService,
    httpInterceptorProviders,
    MatDatepickerModule,
    MutationService,
    QueryService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
