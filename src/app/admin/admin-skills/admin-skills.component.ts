import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ApolloQueryResult } from 'apollo-client';

import { MutationService } from 'src/app/apollo/mutation.service';
import { QueryService } from 'src/app/apollo/query.service';
import { SkillsData } from 'src/app/models/datas';
import { Section } from 'src/app/models/field-base';
import { Skill, SkillUpdate } from 'src/app/models/skill';

@Component({
  selector: 'app-admin-skills',
  templateUrl: './admin-skills.component.html',
  styleUrls: ['./admin-skills.component.css']
})
export class AdminSkillsComponent implements OnInit {
  skills: Skill[];
  skillsSnapshot: Skill[];

  private idsToDelete: number[] = [];
  private skillSection: Section;

  constructor(private mutationService: MutationService,
    private queryService: QueryService,
    private snackBar: MatSnackBar) {
    this.skillSection = require('src/assets/sections/skill');
  }

  ngOnInit(): void {
    this.querySkills();
  }

  addEmptySkill(): void {
    this.skills.splice(0, 0, {missionSkills: []} as Skill);
  }

  /**
   * Supprime la ligne de la liste de skills, en gardant en mémoire son id si le skill était déjà en bdd
   * @param i index de la ligne à supprimer
   */
  onDelete(i: number): void {
    if (this.skills[i].id)
        this.idsToDelete.push(this.skills[i].id);

    if (this.skills.length > 1)
      this.skills.splice(i, 1);
    else
      this.skills[i] = {} as Skill;

    this.orderArrays();
  }

  onReset(): void {
    this.idsToDelete = [];
    this.skills = this.skillsSnapshot.map(s => s);

    this.orderArrays();
  }

  /**
   * Cette méthode vient delete en bdd les lignes supprimées par l'utilisateur
   * Puis update ceux dont le label a été update
   * Puis créer les nouvelles lignes
   */
  onSave(): void {
    if (this.skills.length > 0) {
      var promise1 = null;
      var promise2 = null;
      var promise3 = null;

      if (this.idsToDelete.length > 0) {
        const argumentFieldsIds = [{ key: 'ids', type: '[Int!]', value: this.idsToDelete }];
        promise1 = this.mutationService.mutateSingleRow(this.skillSection.delete, null, argumentFieldsIds);
      }

      const skillsToUpdate = this.skills.filter(s => s.id && s.label)
        .map(s => new SkillUpdate(s));

      if (skillsToUpdate.length > 0) {
        const argumentFieldsSkills = [{ key: 'skills', type: '[updateSkill!]', value: skillsToUpdate }];
        promise2 = this.mutationService.mutateSingleRow(this.skillSection.edit, null, argumentFieldsSkills);
      }

      var skillsToCreate = this.skills.filter(s => !s.id && s.label)
        .filter(s => skillsToUpdate.map(s => s.label).indexOf(s.label) < 0)
        .map(s => s.label);
      skillsToCreate = skillsToCreate.filter((s, i) => skillsToCreate.indexOf(s) === i);

      if (skillsToCreate.length > 0) {
        const argumentFieldsLabels = [{ key: 'labels', type: '[String!]', value: skillsToCreate }];
        promise3 = this.mutationService.mutateSingleRow(this.skillSection.createMultiple, null, argumentFieldsLabels);
      }

      Promise.all([promise1, promise2, promise3])
        .then(() => {
          this.querySkills();
          this.idsToDelete = [];
          this.snackBar.open('Compétences enregistrées.', 'Fermer', { duration: 2000, panelClass: 'success' });
        })
        .catch(() => {
          this.snackBar.open('Echec lors de l\'enregistrement des compétences.', 'Fermer', { duration: 2000, panelClass: 'danger' });
        });
    }
  }

  private orderArrays(): void {
    this.skills = this.skills.sort(this.sort);
    this.skillsSnapshot = this.skillsSnapshot.sort(this.sort);
  }

  private querySkills(): void {
    this.queryService.queryManyRows(this.skillSection.search, this.skillSection.fields)
      .then((skills: ApolloQueryResult<SkillsData<Skill[]>>) => {
        this.skills = skills.data.skills.map(s => s);
        this.skillsSnapshot = skills.data.skills.map(s => s);

        this.orderArrays();
      });
  }

  /**
   * Trie 2 skills par leur label, s'il n'y a pas de label, on met string vide pour que ça se retrouve au début
   */
  private sort(s1: Skill, s2: Skill): number {
    const label1 = s1.label || '';
    const label2 = s2.label || '';

    return label1.toUpperCase() > label2.toUpperCase() ? 1 : label1.toUpperCase() === label2.toUpperCase() ? 0 : -1;
  }
}
