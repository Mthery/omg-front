import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ApolloQueryResult } from 'apollo-client';

import { ClientAlertComponent } from 'src/app/alerts/client-alert/client-alert.component';
import { MutationService } from 'src/app/apollo/mutation.service';
import { QueryService } from 'src/app/apollo/query.service';
import { ClientAlertData } from 'src/app/models/alert';
import { ClientVM, Client, ClientRequest } from 'src/app/models/client';
import { LocationsData } from 'src/app/models/datas';
import { Section } from 'src/app/models/field-base';
import { ClientLocation, Location, LocationCreate, LocationUpdate } from 'src/app/models/location';

@Component({
  selector: 'app-admin-clients',
  templateUrl: './admin-clients.component.html',
  styleUrls: ['./admin-clients.component.css']
})
export class AdminClientsComponent implements OnInit {
  clients: ClientVM[] = [];
  clientsSnapshot: ClientVM[] = [];
  clientIdsToDelete: number[] = [];
  locationIdsToDelete: number[] = [];

  private clientSection: Section;
  private locationSection: Section;

  constructor(private dialog: MatDialog,
    private mutationService: MutationService,
    private snackBar: MatSnackBar,
    private queryService: QueryService) {
    this.clientSection = require('src/assets/sections/client');
    this.locationSection = require('src/assets/sections/location');
  }

  ngOnInit(): void {
    this.queryClientAndLocation();
  }

  addNewClient(): void {
    this.clients.splice(0, 0, {location: []} as ClientVM);
  }
  
  /**
   * Ouverture de la pop-up pour gérer les clients et leurs locations
   */
  onClickClient(client: ClientVM, i: number): void {
    const data = new ClientAlertData(client, []);

    const dialogRef = this.dialog.open(ClientAlertComponent, {
      data: JSON.parse(JSON.stringify(data)),
      disableClose: true,
      width: '40vw'
    });

    dialogRef.afterClosed().subscribe((result: ClientAlertData) => {
      if (result) {
        this.clients[i] = result.client;
        this.locationIdsToDelete = this.locationIdsToDelete.concat(result.idsToDelete);
      }

      this.clients[i].location = this.clients[i].location.filter(l => l.label);
      this.locationIdsToDelete.filter((id, i) => this.locationIdsToDelete.indexOf(id) === i);
      this.orderArrays();
    });
  }

  /**
   * Supprime la ligne de la liste de clients, en gardant en mémoire son id si le client était déjà en bdd
   * @param i index de la ligne à supprimer
   */
  onDelete(i: number): void {
    if (this.clients[i].id)
      this.clientIdsToDelete.push(this.clients[i].id);

    if (this.clients.length > 1)
      this.clients.splice(i, 1);
    else
      this.clients[i] = {location: []} as ClientVM;

    this.orderArrays();
  }

  /**
   * Reset la liste des clients et de leurs locations
   * On utilise 2 map pour reset this.clients, pour ne pas avoir de soucis avec les références d'objet
   */
  onReset(): void {
    this.clientIdsToDelete = [];
    this.locationIdsToDelete = [];
    this.clients = this.clientsSnapshot.map(c => {
      return {
        id: c.id,
        label: c.label,
        location: c.location.map(l => { return {id: l.id, label: l.label} as Location }),
        nbUsed: c.nbUsed
      } as ClientVM;
    });

    this.orderArrays();
  }

  /**
   * Cette méthode vient delete en bdd les lignes supprimées par l'utilisateur
   * Puis update ceux dont le label a été update
   * Puis créer les nouvelles lignes
   */
  onSave(): void {
    if (this.clients.length > 0) {
      var locations: ClientLocation[] = []
      this.clients.forEach(c => locations = locations.concat(c.location.map(l => new ClientLocation(l, c.id))));

      var promises: Promise<any>[] = [];
      promises = promises.concat(this.prepareDeletePromises());
      promises = promises.concat(this.prepareUpdatePromises(locations));
      promises = promises.concat(this.prepareCreatePromise(locations));

      Promise.all(promises)
        .then(() => {
          this.queryClientAndLocation();
          this.clientIdsToDelete = [];
          this.locationIdsToDelete = [];
          this.snackBar.open('Clients enregistrés.', 'Fermer', { duration: 2000, panelClass: 'success' });
        })
        .catch(() => {
          this.snackBar.open('Echec lors de l\'enregistrement des clients.', 'Fermer', { duration: 2000, panelClass: 'danger' });
        });
    }
  }

  private orderArrays(): void {
    this.clients = this.clients.sort(this.sort);
    this.clientsSnapshot = this.clientsSnapshot.sort(this.sort);
  }

  /**
   * Prépare les 2 promises de création
   * La première vient créer les nouveaux clients et leurs locations en même temps
   * La deuxième vient créer les nouvelles locations des clients déjà existants
   * @param locations liste des locations, tout client confondus
   */
  private prepareCreatePromise(locations: ClientLocation[]): Promise<any>[] {
    var promiseCreateClient;
    var promiseCreateLocation;

    const existingClients = this.clients.filter(c => c.id && c.label)
        .map(c => c.label);

    var clientsToCreate = this.clients.filter(c => !c.id && c.label)
      .filter(c => existingClients.indexOf(c.label) < 0)
      .map(c => new ClientRequest(c));
    clientsToCreate = clientsToCreate.filter((c, i) => clientsToCreate.indexOf(c) === i);
      
    if (clientsToCreate.length > 0) {
      clientsToCreate.forEach(client => {
        client.location = client.location.map(loc => {return {label: loc.label} as Location});
      });
      const argumentFieldsClientCreate = [{ key: 'createClientWithLocation', type: '[createClientWithLocation!]', value: clientsToCreate }];
      promiseCreateClient = this.mutationService.mutateSingleRow(this.clientSection.createMultiple, null, argumentFieldsClientCreate);
    }

    const locationsToCreate = locations.filter(l => !l.id && l.label && l.clientId)
      .map(l => new LocationCreate(l));
    
    if (locationsToCreate.length > 0) {
      const argumentFieldsLocationCreate = [{ key: 'createLocations', type: '[createLocation!]', value: locationsToCreate }];
      promiseCreateLocation = this.mutationService.mutateSingleRow(this.locationSection.createMultiple, null, argumentFieldsLocationCreate);
    }

    return [promiseCreateClient, promiseCreateLocation];
  }

  /**
   * Prépare les 2 promises de delete
   * La première vient delete les clients à supprimer ainsi que les locations qui leurs sont liées
   * La deuxième vient delete les locations supprimées qui sont rattachées à des clients toujours existants
   */
  private prepareDeletePromises(): Promise<any>[] {
    var promiseDeleteClient;
    var promiseDeleteLocation;
  
    if (this.clientIdsToDelete.length > 0) {
      const argumentFieldsClientIds = [{ key: 'ids', type: '[Int!]', value: this.clientIdsToDelete }];
      promiseDeleteClient = this.mutationService.mutateSingleRow(this.clientSection.delete, null, argumentFieldsClientIds);
    }

    if (this.locationIdsToDelete.length > 0) {
      const argumentFieldsLocationIds = [{ key: 'ids', type: '[Int!]', value: this.locationIdsToDelete }];
      promiseDeleteLocation = this.mutationService.mutateSingleRow(this.locationSection.delete, null, argumentFieldsLocationIds);
    }

    return [promiseDeleteClient, promiseDeleteLocation];
  }

  /**
   * Prépare les 2 promises d'update
   * La première vient update les clients dont le label a été modifié
   * La seconde vient update les locations dont le label a été modifié
   * @param locations liste des locations, tout client confondus
   */
  private prepareUpdatePromises(locations: ClientLocation[]): Promise<any>[] {
    var promiseUpdateClient;
    var promiseUpdateLocation;

    const clientsToUpdate = this.clients.filter(c => c.id && c.label)
        .map(c => new Client(c));
      
    if (clientsToUpdate.length > 0) {
      const argumentFieldsClienUpdate = [{ key: 'clients', type: '[updateClient!]', value: clientsToUpdate }];
      promiseUpdateClient = this.mutationService.mutateSingleRow(this.clientSection.edit, null, argumentFieldsClienUpdate);
    }
    
    const locationsToUpdate = locations.filter(l => l.id && l.label)
      .map(l => new LocationUpdate(l));
      
    if (locationsToUpdate.length > 0) {
      const argumentFieldsLocationUpdate = [{ key: 'locations', type: '[updateLocation!]', value: locationsToUpdate }];
      promiseUpdateLocation = this.mutationService.mutateSingleRow(this.locationSection.edit, null, argumentFieldsLocationUpdate);
    }

    return [promiseUpdateClient, promiseUpdateLocation];
  }

  /**
   * Vient peupler les tableaux clients et clientSnapshot via un appel back
   * N.B. on utilise ici {id: loc.id, label: loc.label, missions: loc.missions} as Location et pas loc, pour ne pas avoir les même références d'objet
   */
  private queryClientAndLocation(): void {
    this.clients = [];
    this.clientsSnapshot = [];

    this.queryService.queryManyRows(this.locationSection.search, this.locationSection.fields)
      .then((result: ApolloQueryResult<LocationsData<Location[]>>) => {
        result.data.locations.forEach(loc => {
          const client: ClientVM = this.clients.find(c => c.id === loc.client.id && c.label === loc.client.label);

          if (client) {
            this.clients.find(c => c.id === loc.client.id && c.label === loc.client.label).location.push({id: loc.id, label: loc.label, missions: loc.missions} as Location);
            this.clients.find(c => c.id === loc.client.id && c.label === loc.client.label).nbUsed += loc.missions.length;
            this.clientsSnapshot.find(c => c.id === loc.client.id && c.label === loc.client.label).location.push({id: loc.id, label: loc.label, missions: loc.missions} as Location);
            this.clientsSnapshot.find(c => c.id === loc.client.id && c.label === loc.client.label).nbUsed += loc.missions.length;
          }
          else {
            this.clients.push(new ClientVM(loc.client.id, loc.client.label, {id: loc.id, label: loc.label, missions: loc.missions} as Location));
            this.clientsSnapshot.push(new ClientVM(loc.client.id, loc.client.label, {id: loc.id, label: loc.label, missions: loc.missions} as Location));
          }
        });

        this.orderArrays();
      });
  }

  /**
   * Trie 2 clients par leur label, s'il n'y a pas de label, on met string ide pour que ça se retrouve au début
   */
  private sort(c1: ClientVM, c2: ClientVM): number {
    const label1 = c1.label || '';
    const label2 = c2.label || '';

    return label1.toUpperCase() > label2.toUpperCase() ? 1 : label1.toUpperCase() === label2.toUpperCase() ? 0 : -1;
  }
}
