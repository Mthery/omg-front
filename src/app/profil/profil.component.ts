import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ApolloQueryResult } from 'apollo-client';
import { first } from 'rxjs/operators';

import { MutationService } from 'src/app/apollo/mutation.service';
import { QueryService } from 'src/app/apollo/query.service';
import { PersonData } from 'src/app/models/datas';
import { Section } from 'src/app/models/field-base';
import { EditPerson, Person } from 'src/app/models/person';
import { AuthService } from 'src/app/welcome-page/auth.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {
  form: FormGroup = this.fb.group({
    email: ['', [Validators.required, Validators.pattern('^[a-zA-Z0-9._%+-]+@econocom\.com$')]],
    firstname: ['', Validators.required],
    lastname: ['', Validators.required],
    phone: ['', [Validators.minLength(10), Validators.maxLength(10)]]
  });
  pwdForm: FormGroup = this.fb.group({
    password: ['', Validators.required]
  })

  person: Person;
  pwdReadable: boolean = false;

  private authSection: Section;
  private hashage = require('sha.js');
  private personSection: Section;
  private userId: number;

  constructor(private authService: AuthService,
      private fb: FormBuilder,
      private mutationService: MutationService,
      private queryService: QueryService,
      private snackBar: MatSnackBar) {
    this.authSection = require('src/assets/sections/auth');
    this.personSection = require('src/assets/sections/person');
  }

  ngOnInit(): void {
    this.authService.userIdSubject
      .pipe(first())
      .subscribe(id => {
        this.userId = id;
        this.getPerson();
      });

    this.authService.emitUserId();
  }

  /**
   * Vient mettre à jour la table Authentification (email + password)
   */
  onSubmitAuth(): void {
    const email = this.hashage('sha256').update(this.person.email).digest('hex');
    const password = this.hashage('sha256').update(this.pwdForm.value.password).digest('hex');

    const argumentFieldsPersonId = { key: 'person_id', type: 'Int!', value: this.userId };
    const argumentFieldsEmail = { key: 'email', type: 'String!', value: email };
    const argumentFieldsPassword = { key: 'password', type: 'String!', value: password };

    this.mutationService.mutateSingleRow(this.authSection.edit, null, [argumentFieldsPersonId, argumentFieldsEmail, argumentFieldsPassword])
    .then(() => {
      this.snackBar.open('Mot de passe enregistré.', 'Fermer', { duration: 2000, panelClass: 'success' });
      this.pwdForm.reset();
    })
    .catch(() => {
      this.snackBar.open('Echec lors de l\'enregistrement du mot de passe.', 'Fermer', { duration: 2000, panelClass: 'danger' });
    });
  }

  /**
   * Vient mettre à jour la table Person, puis l'adresse email présente dans la table Authentification
   */
  onSubmitProfil(): void {
    const nom = this.form.value.lastname as string;
    const prenom = this.form.value.firstname as string;
    const email = this.form.value.email as string;
    const phone = this.form.value.phone as string;

    const editPerson = new EditPerson(nom, prenom, email, phone);

    const argumentFieldsEditPerson = { key: this.personSection.edit, type: this.personSection.edit, value: editPerson };
    const argumentFieldsId = { key: 'id', type: 'Int!', value: this.userId };

    this.mutationService.mutateSingleRow(this.personSection.edit, null, [argumentFieldsId, argumentFieldsEditPerson])
      .then(() => {
        this.getPerson();

        const emailHash = this.hashage('sha256').update(email).digest('hex');
        const argumentFieldsEmail = { key: 'email', type: 'String!', value: emailHash };
        const argumentFieldsPersonId = { key: 'person_id', type: 'Int!', value: this.userId };

        this.mutationService.mutateSingleRow(this.authSection.edit, null, [argumentFieldsPersonId, argumentFieldsEmail])
          .then(() => {
            this.snackBar.open('Profil enregistré.', 'Fermer', { duration: 2000, panelClass: 'success' });
          })
          .catch(() => {
            this.snackBar.open('Echec lors de l\'enregistrement du profil.', 'Fermer', { duration: 2000, panelClass: 'danger' });
          });
      })
      .catch(() => {
        this.snackBar.open('Echec lors de l\'enregistrement du profil.', 'Fermer', { duration: 2000, panelClass: 'danger' });
      });
  }

  resetForm(): void {
    this.form.patchValue({
      email: this.person.email,
      firstname: this.person.firstname,
      lastname: this.person.lastname,
      phone: this.person.phone
    });
  }

  private getPerson(): void {
    this.queryService.querySingleRow(this.personSection.detail, this.personSection.fields, this.userId.toString())
      .then((result: ApolloQueryResult<PersonData<Person>>) => {
        this.person = result.data.person;
        this.resetForm();
      });
  }
}
