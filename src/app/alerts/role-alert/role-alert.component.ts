import { Component, Inject } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { BottomSheetComponent } from 'src/app/alerts/bottom-sheet/bottom-sheet.component';
import { Profil } from 'src/app/constants/profil';
import BottomSheetResponse from 'src/app/models/bottomSheetResponse';
import { Person } from 'src/app/models/person';

@Component({
  selector: 'app-role-alert',
  templateUrl: './role-alert.component.html',
  styleUrls: ['./role-alert.component.css']
})
export class RoleAlertComponent {
  profils = Profil;

  constructor(private bottomSheet: MatBottomSheet,
    private dialogRef: MatDialogRef<RoleAlertComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Person) {}

  onCancel(): void {
    const alert = this.bottomSheet.open(BottomSheetComponent, { data: 'Êtes-vous certain(e) de vouloir fermer cette fenêtre sans avoir sauvegardé ?' });
    alert.afterDismissed()
      .subscribe((data: BottomSheetResponse) => {
        if (data && data.message === "Ok") {
          this.dialogRef.close();
        }
      });
  }

  canSave(): boolean {
    return this.data.profil && this.profils[this.data.profil] !== this.profils.ADMIN;
  }
}
