import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ApolloQueryResult } from 'apollo-client';
import { ExportToCsv } from 'export-to-csv';

import { MutationService } from 'src/app/apollo/mutation.service';
import { QueryService } from 'src/app/apollo/query.service';
import { Agencies } from 'src/app/constants/agencies';
import { Levels } from 'src/app/constants/levels';
import { Priorities } from 'src/app/constants/priorities';
import { Status } from 'src/app/constants/status';
import { Types } from 'src/app/constants/types';
import { CreateMissionData, LocationsData, PersonByNameData, SkillsData } from 'src/app/models/datas';
import { Section } from 'src/app/models/field-base';
import { Location } from 'src/app/models/location';
import { MissionCandidate, MissionRequest } from 'src/app/models/mission';
import { MissionSkillRequest } from 'src/app/models/missionSkill';
import { Person } from 'src/app/models/person';
import { Skill } from 'src/app/models/skill';

@Component({
  selector: 'app-import-alert',
  templateUrl: './import-alert.component.html',
  styleUrls: ['./import-alert.component.css']
})
export class ImportAlertComponent implements OnInit {
  candidates: MissionCandidate[][] = [];
  errorByLines: String[][];
  locations: Location[];
  missions: MissionRequest[] = [];
  missionSkills: MissionSkillRequest[][] = [];
  showErrors: boolean[];
  skills: Skill[];

  private candidateSection: Section;
  private locationSection: Section;
  private missionSection: Section;
  private missionSkillSection: Section;
  private personSection: Section;
  private skillSection: Section;

  constructor(public dialogRef: MatDialogRef<ImportAlertComponent>,
    private mutationService: MutationService,
    private queryService: QueryService,
    private snackBar: MatSnackBar) {
      this.candidateSection = require("src/assets/sections/candidate");
      this.locationSection = require("src/assets/sections/location");
      this.missionSection = require("src/assets/sections/mission");
      this.missionSkillSection = require("src/assets/sections/missionSkill");
      this.personSection = require("src/assets/sections/person");
      this.skillSection = require("src/assets/sections/skill");
    }

  ngOnInit(): void {
    this.getData();
  }

  /**
   * Crée et fait télécharger à l'utilisateur un fichier csv d'exemple
   */
  onClickDownload(): void {
    const csvData = [{
      "Date*": "Année-mois-jour",
      "IC/BM*": "Prénom Nom",
      "Client*": "Nom du client",
      "Agence*": "Numéro d'agence (521/522/523)",
      "Localisation*": "Localisation du client",
      "Poste/Profil*": "Description de l'offre",
      "Priorité*": "Priorité de l'offre (P1/P2/P3)",
      "Statut*": "Statut de l'offre (AO ouvert/AO perdu/...)",
      "Type": "Type de l'offre (Infogérance ou Assistance tech.)",
      "TJM": "500",
      "Démarrage mission": "Année-mois-jour",
      "Expérience": "Expérience recherchée (X < 2/2 < X < 4/...)",
      "Collaborateur à remplacer": "Prénom Nom",
      "Candidat(s) (séparés par un ;)": "Prénom Nom;Prénom Nom;Prénom Nom",
      "Compétence(s) (séparés par un ;)": "Compétence;Compétence"
    }];
    const options = {
      quoteStrings: '',
      showLabels: true,
      filename: 'exemple_import',
      useKeysAsHeaders: true
    }

    const csvExporter = new ExportToCsv(options);
    csvExporter.generateCsv(csvData);
  }

  /**
   * Récupère le fichier importé par l'utilisateur, le vérifie et prépare les objets MissionRequest pour la création
   */
  onFileSelected(): void {
    const input: HTMLInputElement = document.querySelector('#file');
    const reader = new FileReader();
    this.errorByLines = [];
    this.showErrors = [];
    this.missions = [];

    if (input.files[0] && input.files[0].name.endsWith(".csv")) {
      reader.readAsText(input.files[0], 'ISO-8859-1');
      reader.onload = async () => {
        const data = reader.result;
        let dataArray = (<string> data).split(/\r\n|\n/);

        for (let i = 1; i < dataArray.length; i ++) {
          const line = dataArray[i].split(",");

          if (line.every(val => val === ""))
            continue;

          const mission = {} as MissionRequest;
          const candidates: MissionCandidate[] = [];

          mission.date_in = line[0];
          await this.findPersonByName(line[1], mission, "created_by", "IC/BM", null, i);
          mission.agency = line[3];
          mission.description = line[5];
          mission.priority = line[6];
          mission.status = this.findStatusByValue(line[7]);
          mission.type = this.findTypeByValue(line[8]);

          if (line[9] && line[9] !== "")
            mission.adr = line[9];

          if (line[10] && line[10] !== "")
            mission.date_start = line[10];

          if (this.locations.find(loc => loc.client.label === line[2] && loc.label === line[4]))
            mission.client_location_id = this.locations.find(loc => loc.client.label === line[2] && loc.label === line[4]).id;
          else
            this.addError(`Le client renseigné (${line[2]}) n'existe pas en base de donnée, ou ${line[4]} ne se trouve pas dans ses localisations`, i-1);

          if (line[12] && line[12] !== "")
            await this.findPersonByName(line[12], mission, "replaced", "collaborateur", null, i);

          if (line[13] && line[13] !== "") {
            line[13].split(";").forEach(async c => {
              await this.findPersonByName(c.trim(), null, null, "collaborateur", candidates, i);
            });
            this.candidates[i-1] = candidates;
          }

          if (line[14] && line[14] !== "") {
            this.findSkills(line[14].split(";"), line[11], i);
          }

          this.validateMission(mission, i, line[7], line[8]);
          this.missions.push(mission);
        }
      };
    } else {
      this.snackBar.open('Veuillez sélectionner un fichier .csv', 'Fermer', { duration: 2000, panelClass: 'danger' });
    }

    input.value = "";
    for (let i = 0; i < this.errorByLines.length; i ++) {
      this.showErrors[i] = false;
    }
  }

  /**
   * Crée les offres en bdd
   */
  sendRequest(): void {
    const promises: Promise<any>[] = [];
    this.missions.forEach((mission, i) => {
      const argumentFieldMission = { key: this.missionSection.create, type: this.missionSection.create, value: mission };

      promises.push(this.mutationService.mutateSingleRow(this.missionSection.create, null, [argumentFieldMission])
        .then((created: ApolloQueryResult<CreateMissionData>) => {
          if (this.candidates[i] && this.candidates[i].length > 0) {
            this.candidates[i].forEach(c => {
              c.mission_id = created.data.createMission;
            });

            const argumentFieldCandidate = { key: this.candidateSection.create, type: `[${this.candidateSection.create}!]`, value: this.candidates[i] };
            this.mutationService.mutateSingleRow(this.candidateSection.create, null, [argumentFieldCandidate]);
          }

          if (this.missionSkills[i] && this.missionSkills[i].length > 0) {
            this.missionSkills[i].forEach(m => {
              m.mission_id = created.data.createMission;
            });

            const argumentFieldMissionSkill = { key: this.missionSkillSection.create, type: `[${this.missionSkillSection.create}!]`, value: this.missionSkills[i] };
            this.mutationService.mutateSingleRow(this.missionSkillSection.create, null, [argumentFieldMissionSkill]);
          }
        }));
    });

    Promise.all(promises)
      .then(() => {
        this.snackBar.open('Enregistrement des offres terminé', 'Fermer', { duration: 2000, panelClass: 'success' });
        this.dialogRef.close();
      })
      .catch(() => {
        this.snackBar.open('Erreur lors de l\'enregistrement des offres', 'Fermer', { duration: 2000, panelClass: 'danger' });
      });
  }

  /**
   * Ajoute une erreur à la liste
   * @param error Le message d'erreur
   * @param index Le numéro de la ligne en erreur - 2 (-1 car tableau commence à 0, et -1 à cause de la ligne de header)
   */
  private addError(error: string, index: number): void {
    if (!this.errorByLines[index])
      this.errorByLines[index] = [];

    this.errorByLines[index].push(error);
  }

  /**
   * Retrouve une personne par ses nom et prénom (il faut que ça soit bien dans cet ordre)
   * @param name Nom et prénom de la personne
   * @param mission L'objet auquel ajouté la personne trouvée
   * @param property La propriété de l'objet qui sera setté
   * @param role Rôle de la personnne recherchée (utile dans le message d'erreur)
   * @param candidates Liste de candidats, si cette variable n'est pas null elle est utilisée à la place de mission et property
   * @param i Numéro de la ligne - 1 (-1 car tableau commence à 0)
   */
  private async findPersonByName(name: string, mission: MissionRequest, property: string, role: string, candidates: MissionCandidate[], i: number): Promise<void> {
    const names = name.split(" ");
    const argumentFieldFirstname = { key: "firstname", type: "String", value: names[0]};
    const argumentFieldLastname = { key: "lastname", type: "String", value: names[1]};

    await this.queryService.queryManyRowsWithArgs("personByName", this.personSection.fields.filter(f => f.key === "id"), [argumentFieldLastname, argumentFieldFirstname])
      .then((result: ApolloQueryResult<PersonByNameData<Person>>) => {
        if (result.data.personByName)
          if (candidates)
            candidates.push({collaborator_id: result.data.personByName.id} as MissionCandidate);
          else
            mission[property] = result.data.personByName.id;
        else
          this.addError(`Aucun ${role} trouvé ayant comme prénom ${names[0]} et comme nom ${names[1]}`, i-1);
      });
  }

  /**
   * Crée des objets MissionSkillRequest à partir d'une liste de compétence (label) et d'une expérience requise
   * @param skillList La liste de compétence venant du csv
   * @param xp L'expérience requise venant du csv
   * @param skills Le tableau dans lequel stocker les objets
   * @param i Numéro de la ligne - 1 (-1 car tablea commence à 0)
   */
  private findSkills(skillList: string[], xp: string, i: number): void {
    this.missionSkills[i-1] = [];

    const level = this.formatLevel(xp);
    if (!level)
      this.addError(`L'expérience renseignée (${xp}) n'est pas correcte (options disponibles: ${Object.keys(Levels).join(', ')})`, i-1);

    skillList.forEach(skillLabel => {
      const skill = this.skills.find(s => s.label === skillLabel.trim());

      if (skill)
        this.missionSkills[i-1].push({level, mandatory: true, skill_id: skill.id} as MissionSkillRequest);
      else
        this.addError(`La compétence renseignée (${skillLabel}) n'existe pas en base de données`, i-1);
    });
  }

  /**
   * Transforme un status en sa clé (ex: Ao ouvert -> OPEN)
   * @param value le status à transformer
   * @returns la clé
   */
  private findStatusByValue(value: string): string {
    let status: string = null;

    Object.keys(Status).forEach(key => {
      if (Status[key] === value) {
        status = key;
      }
    });

    return status;
  }

  /**
   * Transforme un type en sa clé (ex: Infogérance -> INFO)
   * @param value le type à transformer
   * @returns la clé
   */
  private findTypeByValue(value: string): string {
    let type: string = Object.keys(Types)[0];

    Object.keys(Types).forEach(key => {
      if (Types[key] === value) {
        type = key;
      }
    });

    return type;
  }

  /**
   * Transforme un niveau requis (venant du csv) en level (enum de la bdd)
   * @param xp Niveau requis venant du csv
   * @return Level insérable en bdd
   */
  private formatLevel(xp: string): string {
    let level = null;

    (<any>Object).keys(Levels).forEach(l => {
      if (l === xp)
        level = Levels[l];
    });

    return level;
  }

  /**
   * Récupère les locations, clients et skills de la bdd
   */
  private getData(): void {
    this.queryService.queryManyRows(this.locationSection.search, this.locationSection.fields)
      .then((result: ApolloQueryResult<LocationsData<Location[]>>) => {
        this.locations = result.data.locations;
      });

    this.queryService.queryManyRows(this.skillSection.search, this.skillSection.fields)
      .then((result: ApolloQueryResult<SkillsData<Skill[]>>) => {
        this.skills = result.data.skills;
      });
  }

  /**
   * Valide les données de l'offre (qui ne sont pas déjà validées dans d'autres méthodes)
   * @param mission L'offre à valider
   * @param i Numéro de la ligne - 1 (-1 car tableau commence à 0)
   * @param status Le statut entré dans le csv (s'il n'est pas bon, mission.status vaudra null)
   * @param type Le type de mission entré dans le csv (s'il n'est pas bon, mission.type vaudra null)
   */
  private validateMission(mission: MissionRequest, i: number, status: string, type: string): void {
    if ((<any> Object).values(Agencies).indexOf(mission.agency) < 0)
      this.addError(`L'agence renseignée (${mission.agency}) n'est pas correcte (options disponibles : ${Object.values(Agencies).join(', ')})`, i-1);

    if ((<any> Object).keys(Priorities).indexOf(mission.priority) < 0)
      this.addError(`La priorité renseignée (${mission.priority}) n'est pas correcte (options disponibles : ${Object.keys(Priorities).join(', ')})`, i-1);

    if (!mission.status)
      this.addError(`Le statut renseigné (${status}) n'est pas correct (options disponibles : ${Object.values(Status).join(', ')})`, i-1);
  }
}
