import { Component, Inject, OnInit } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApolloQueryResult } from 'apollo-client';

import { BottomSheetComponent } from 'src/app/alerts/bottom-sheet/bottom-sheet.component';
import { QueryService } from 'src/app/apollo/query.service';
import { CandidateAlertData } from 'src/app/models/alert';
import BottomSheetResponse from 'src/app/models/bottomSheetResponse';
import { CandidatesSuggestionsData, CollaboratorsData } from 'src/app/models/datas';
import { Section } from 'src/app/models/field-base';
import { Person, Candidate } from 'src/app/models/person';

@Component({
  selector: 'app-candidate-alert',
  templateUrl: './candidate-alert.component.html',
  styleUrls: ['./candidate-alert.component.css']
})
export class CandidateAlertComponent implements OnInit {
  candidatesSuggestions: Candidate[];
  collaborators: Person[] = [];
  personSuggestions: Person[] = [];
  search: string = '';

  private candidateSection: Section;
  private collaboratorSection: Section;

  constructor(private bottomSheet: MatBottomSheet,
    private dialogRef: MatDialogRef<CandidateAlertComponent>,
    private queryService: QueryService,
    @Inject(MAT_DIALOG_DATA) public data: CandidateAlertData) {
      this.candidateSection = require("src/assets/sections/candidate");
      this.collaboratorSection = require("src/assets/sections/collaborator");
    }

  ngOnInit(): void {
    this.queryService.queryManyRows(this.collaboratorSection.search, this.collaboratorSection.fields.filter(f => ["id", "lastname", "firstname"].indexOf(f.key) >= 0))
      .then((resultCollab: ApolloQueryResult<CollaboratorsData<Person[]>>) => {
        this.collaborators = resultCollab.data.collaborators;
        this.personSuggestions = this.collaborators;

        const argumentFieldsSkills = [{key: 'skills', type: '[skillAndLevel!]', value: this.data.missionSkills}];
        this.queryService.queryManyRowsWithArgs(this.candidateSection.search, this.candidateSection.fields, argumentFieldsSkills)
          .then((resultSuggestion: ApolloQueryResult<CandidatesSuggestionsData<Candidate[]>>) => {
            this.candidatesSuggestions = resultSuggestion.data.candidatesSuggestions;
            
            if (this.candidatesSuggestions)
              this.candidatesSuggestions = this.candidatesSuggestions.sort(this.sortPersons);
          });

        this.filterSuggestions();
      });

    this.data.candidates = this.data.candidates.sort(this.sortPersons);
  }

  candidateAlreadySelected(candidate: Person | Candidate): boolean {
    return this.data.candidates.find(c => c.id === candidate.id) ? true: false;
  }

  displayFunction(val: Person): string {
    return val ? `${val.lastname} ${val.firstname}` : '';
  }

  onCancel(): void {
    if (!this.data.readOnly) {
      const alert = this.bottomSheet.open(BottomSheetComponent, { data: 'Êtes-vous certain(e) de vouloir fermer cette fenêtre sans avoir sauvegardé ?' });
      alert.afterDismissed()
        .subscribe((data: BottomSheetResponse) => {
          if (data && data.message === "Ok") {
            this.dialogRef.close();
          }
        });
    } else {
      this.dialogRef.close();
    }
  }

  onClickCandidate(candidate: Candidate) {
    if (!this.candidateAlreadySelected(candidate)) {
      const candidatePerson = this.collaborators.find(c => c.id === candidate.id);
      this.data.candidates.push(candidatePerson);
      this.data.candidates = this.data.candidates.sort(this.sortPersons);
    }
  }

  onDelete(i: number): void {
    this.data.candidates.splice(i, 1);
  }

  /**
   * Vient sélectionner les personnes correspondant à l'input pour les proposer en autocomplete
   */
  onSearch(): void {
    const searchWords: string[] = this.search.split(" ");
    searchWords.forEach(word => word = word.toUpperCase());
    this.personSuggestions = [];

    searchWords.forEach(word => {
      this.personSuggestions = this.personSuggestions.concat(
        this.collaborators
          .filter(p => p.firstname.toUpperCase().startsWith(word.toUpperCase()) || p.lastname.toUpperCase().startsWith(word.toUpperCase()))
          .sort(this.sortPersons));
    });

    this.filterSuggestions();
  }

  onSelect(val: Person): void {
    this.data.candidates.push(val);
    this.data.candidates = this.data.candidates.sort(this.sortPersons);
  }

  /**
   * Permet d'enlever les doublons, et de ne plus proposer les candidats déjà sélectionnés
   */
  private filterSuggestions = (): void => {
    this.personSuggestions = this.personSuggestions
      .filter((p, index, array) => array.indexOf(p) === index)
      .filter(p => !this.candidateAlreadySelected(p))
      .sort(this.sortPersons);
  }

  /**
   * Méthode à passer à .sort pour trier un tableau de Person par nom, par ordre alphabétique
   */
  private sortPersons(p1: Person | Candidate, p2: Person | Candidate): number {
    if (p1.lastname.toUpperCase() > p2.lastname.toUpperCase())
      return 1;
    
    if (p1.lastname.toUpperCase() < p2.lastname.toUpperCase())
      return -1;

    return 0;
  }
}

