import { Component, Inject, OnInit } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ApolloQueryResult } from 'apollo-client';

import { BottomSheetComponent } from 'src/app/alerts/bottom-sheet/bottom-sheet.component';
import { CreateSkillAlertComponent } from 'src/app/alerts/create-skill-alert/create-skill-alert.component';
import { QueryService } from 'src/app/apollo/query.service';
import { Levels } from 'src/app/constants/levels';
import { SkillAlertData } from 'src/app/models/alert';
import BottomSheetResponse from 'src/app/models/bottomSheetResponse';
import { SkillsData } from 'src/app/models/datas';
import { MissionSkillRequest } from 'src/app/models/missionSkill';
import { Section } from 'src/app/models/field-base';
import { Skill } from 'src/app/models/skill';

@Component({
  selector: 'app-skill-alert',
  templateUrl: './skill-alert.component.html',
  styleUrls: ['./skill-alert.component.css']
})
export class SkillAlertComponent implements OnInit {
  levels = Levels;
  skills: Skill[];
  
  private skillSection: Section;

  constructor(private bottomSheet: MatBottomSheet,
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<SkillAlertComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SkillAlertData,
    private queryService: QueryService) {
      this.skillSection = require('src/assets/sections/skill');
    }

  ngOnInit(): void {
    this.querySkills();
  }

  addNewMissionSkill(): void {
    if (!this.data.readOnly)
      this.data.missionSkills.push({} as MissionSkillRequest);
  }

  onCancel(): void {
    if (!this.data.readOnly) {
      const alert = this.bottomSheet.open(BottomSheetComponent, { data: 'Êtes-vous certain(e) de vouloir fermer cette fenêtre sans avoir sauvegardé ?' });
      alert.afterDismissed()
        .subscribe((data: BottomSheetResponse) => {
          if (data && data.message === "Ok") {
            this.dialogRef.close();
          }
        });
    } else {
      this.dialogRef.close();
    }
  }

  onDelete(i: number): void {
    this.data.missionSkills.splice(i, 1);
  }

  onNewSkill(): void {
    const dialogRef = this.dialog.open(CreateSkillAlertComponent, {
      disableClose: true,
      width: '30vw'
    });

    dialogRef.afterClosed().subscribe(() => {
      this.querySkills();
    });
  }

  private orderSkills(s1: Skill, s2: Skill): number {
    if (s1.label.toUpperCase() > s2.label.toUpperCase())
      return 1;

    if (s1.label.toUpperCase() < s2.label.toUpperCase())
      return -1;
    
    return 0;
  }

  private querySkills(): void {
    this.queryService.queryManyRows(this.skillSection.search, this.skillSection.fields)
      .then((result: ApolloQueryResult<SkillsData<Skill[]>>) => {
        this.skills = result.data.skills;
        this.skills.sort(this.orderSkills);
      });
  }
}
