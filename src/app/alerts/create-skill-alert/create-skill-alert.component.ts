import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ApolloQueryResult } from 'apollo-client';

import { MutationService } from 'src/app/apollo/mutation.service';
import { QueryService } from 'src/app/apollo/query.service';
import { SkillsData } from 'src/app/models/datas';
import { Section } from 'src/app/models/field-base';
import { Skill } from 'src/app/models/skill';

@Component({
  selector: 'app-create-skill-alert',
  templateUrl: './create-skill-alert.component.html',
  styleUrls: ['./create-skill-alert.component.css']
})
export class CreateSkillAlertComponent {
  matchingSkills: string[] = [];
  skill: string;
  skillAlreadyExists: boolean = false;
  skills: string[];

  private skillSection: Section;

  constructor(private dialogRef: MatDialogRef<CreateSkillAlertComponent>,
    private mutationService: MutationService,
    private queryService: QueryService,
    private snackBar: MatSnackBar) {
    this.skillSection = require('src/assets/sections/skill');
  }

  ngOnInit(): void {
    this.queryService.queryManyRows(this.skillSection.search, this.skillSection.fields)
      .then((result: ApolloQueryResult<SkillsData<Skill[]>>) => {
        this.skills = result.data.skills.map(s => s.label.toUpperCase());
      });
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onInput(): void {
    this.skillAlreadyExists = this.skills.indexOf(this.skill.toUpperCase()) >= 0;

    if (this.skill !== '') {
      this.matchingSkills = this.skills.filter(s => s.indexOf(this.skill.toUpperCase()) >= 0)
        .map(s => this.formatSkill(s));
      if (this.matchingSkills.length > 5)
        this.matchingSkills.splice(5);
    } else {
      this.matchingSkills = [];
    }
  }

  onSave(): void {
    if (this.skill === '' || !this.skill)
      this.dialogRef.close();
    else {
      if (!this.skillAlreadyExists) {
        const argumentField = [{ key: 'label', type: 'String', value: this.skill }];
        this.mutationService.mutateSingleRow(this.skillSection.create, null, argumentField)
          .then(() => {
            this.snackBar.open('Compétence créée.', 'Fermer', { duration: 2000, panelClass: 'success' });
            this.dialogRef.close();
          })
          .catch(() => {
            this.snackBar.open('Erreur lors de la création de la compétence.', 'Fermer', { duration: 2000, panelClass: 'danger' });
          });
      }
    }
  }

  private formatSkill(skill: string): string {
    const splitByWords = skill.split(' ');
    let formattedSkill: string = '';

    splitByWords.forEach(word => {
      formattedSkill += word.charAt(0).toUpperCase() + word.substr(1).toLowerCase() + ' ';
    });

    return formattedSkill.trim();
  }
}
