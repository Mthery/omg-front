import { Component, Inject } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { BottomSheetComponent } from 'src/app/alerts/bottom-sheet/bottom-sheet.component';
import { ClientAlertData } from 'src/app/models/alert';
import BottomSheetResponse from 'src/app/models/bottomSheetResponse';
import { Location } from 'src/app/models/location';

@Component({
  selector: 'app-client-alert',
  templateUrl: './client-alert.component.html',
  styleUrls: ['./client-alert.component.css']
})
export class ClientAlertComponent {
  constructor(private bottomSheet: MatBottomSheet,
    private dialogRef: MatDialogRef<ClientAlertComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ClientAlertData) {}

  addEmptyLocation(): void {
    this.data.client.location.push({missions: []} as Location);
  }

  isDataCorrect(): boolean {
    return this.data.client.location.length > 0 && this.data.client.location.filter(l => l.label && l.label !== '').length > 0;
  }

  onCancel(): void {
    const alert = this.bottomSheet.open(BottomSheetComponent, { data: 'Êtes-vous certain(e) de vouloir fermer cette fenêtre sans avoir sauvegardé ?' });
    alert.afterDismissed()
      .subscribe((data: BottomSheetResponse) => {
        if (data && data.message === "Ok") {
          this.dialogRef.close();
        }
      });
  }

  onDelete(i: number) {
    if (this.data.client.location[i].id)
      this.data.idsToDelete.push(this.data.client.location[i].id);

    this.data.client.location.splice(i, 1);
  }
}
