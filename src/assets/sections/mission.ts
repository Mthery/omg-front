const mission = {
  detail: "mission",
  search: "missions",
  edit: "editMission",
  create: "createMission",
  delete: "deleteMission",
  fields: [
    {
      key: "id"
    },
    {
      key: "client",
      queryOptions: {
        sub_selection: [
          { key: "id" },
          { key: "label" }
        ]
      }
    },
    {
      key: "client_location",
      queryOptions: {
        sub_selection: [
          { key: "id" },
          { key: "label" }
        ]
      }
    },
    {
      key: "description"
    },
    {
      key: "type"
    },
    {
      key: "status"
    },
    {
      key: "priority"
    },
    {
      key: "adr"
    },
    {
      key: "date_start"
    },
    {
      key: "date_out"
    },
    {
      key: "agency"
    },
    {
      key: "replaced",
      queryOptions: {
        sub_selection: [
          { key: "id" },
          { key: "firstname" },
          { key: "lastname" }
        ]
      }
    },
    {
      key: "created_by",
      queryOptions: {
        sub_selection: [
          { key: "lastname" },
          { key: "firstname" }
        ]
      },
    },
    {
      key: "candidates",
      queryOptions: {
        sub_selection: [
          { key: "id" },
          { key: "firstname" },
          { key: "lastname" }
        ]
      }
    },
    {
      key: "skills",
      queryOptions: {
        sub_selection: [
          {
            key: "skill",
            queryOptions: {
              sub_selection: [
                { key : "label" }
              ]
            }
          }
        ]
      }
    },
    {
      key: "contact_email"
    },
    {
      key: "contact_phone"
    }
  ]
};

module.exports = mission;
