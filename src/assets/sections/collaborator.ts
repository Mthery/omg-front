const collaborator = {
  search: "collaborators",
  fields: [
    {
      key: "id"
    },
    {
      key: "firstname"
    },
    {
      key: "lastname"
    }
  ]
};

module.exports = collaborator;
