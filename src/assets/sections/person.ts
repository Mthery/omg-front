const person = {
  detail: "person",
  search: "persons",
  edit: "editPerson",
  create: "createPerson",
  delete: "deletePerson",
  searchLabel: "Personnes",
  fields: [
    {
      key: "id"
    },
    {
      key: "lastname"
    },
    {
      key: "firstname"
    },
    {
      key: "profil"
    },
    {
      key: "email"
    },
    {
      key: "phone"
    },
    {
      key: "client",
      queryOptions: {
        sub_selection: [
          {key: "id"},
          {key: "label"}
        ]
      }
    },
    {
      key: "client_location",
      queryOptions: {
        sub_selection: [
          {key: "id"},
          {key: "label"}
        ]
      }
    },
    {
      key: "manager",
      queryOptions: {
        sub_selection: [
          {key: "id"},
          {key: "lastname"},
          { key: "firstname" }
        ]
      }
    },
    {
      key: "cv"
    },
    {
      key: "date_in"
    },
    {
      key: "adr"
    }
  ]
};

module.exports = person;
