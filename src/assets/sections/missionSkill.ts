const missionSkill = {
  search: "missionSkills",
  create: "createMissionSkill",
  delete: "deleteMissionSkills",
  fields: [
    {
      key: "id"
    },
    {
      key: "mission",
      queryOptions: {
        sub_selection: [
          { key: "id" }
        ]
      }
    },
    {
      key: "skill",
      queryOptions: {
        sub_selection: [
          { key: "id" }
        ]
      }
    },
    {
      key: "level"
    },
    {
      key: "mandatory"
    }
  ]
};

module.exports = missionSkill;
