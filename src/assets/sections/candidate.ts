const candidate = {
  create: "addMissionCandidate",
  delete: "removeMissionCandidate",
  search: "candidatesSuggestions",
  fields: [
    {
      key: "id"
    },
    {
      key: "lastname"
    },
    {
      key: "firstname"
    },
    {
      key: "percentage"
    }
  ]
};

module.exports = candidate;
