const locations = {
  detail: "location",
  search: "locations",
  create: "createLocation",
  createMultiple: "createLocations",
  delete: "deleteLocations",
  edit: "updateLocations",
  fields: [
    {
      key: "id"
    },
    {
      key: "label"
    },
    {
      key: "client",
      queryOptions: {
        sub_selection: [
          {key: "id"},
          {key: "label"}
        ]
      },
    },
    {
      key: "missions",
      queryOptions: {
        sub_selection: [
          {key: "id"}
        ]
      },
    }
  ]
};

module.exports = locations;
