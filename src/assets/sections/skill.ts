const skill = {
  search: "skills",
  create: "createSkill",
  edit: "updateSkills",
  delete: "deleteSkills",
  createMultiple: "createSkills",
  fields: [
    {
      key: "id"
    },
    {
      key: "label"
    },
    {
      key: "missionSkills",
      queryOptions: {
        sub_selection: [
          { key: "id" }
        ]
      }
    }
  ]
};

module.exports = skill;
