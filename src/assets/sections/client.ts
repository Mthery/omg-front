const client = {
  delete: "deleteClients",
  edit : "updateClients",
  createMultiple: "createClientsWithLocations",
};

module.exports = client;
