# Outil de management et gestion

Cette documentation concerne le front de l'outil OMG.

## Module

### Configuration

```javascript
const configuration = {
    detail: "nom de la query d'une entité",
    search: "nom de la query pour plusieurs entités",
    edit: "nom de la mutation pour mettre à jour une entité",
    create: "nom de la mutation pour créer une entité",
    searchLabel: "nom affiché dans un listing d'entités",
    fields: [],
};
```

### Fields

```javascript
const configuration = {
    fields: [
        {
            key: "Identifiant",
            label: "Nom affiché pour le champ",
            required: "Par défaut à vrai, vrai si une valeur est recquise",
            readonly: "Par défaut à faux, vrai si la valeur n'est pas modifiable",
            create: "Par défaut à vrai, vrai si le champ est affiché lors de la création d'une entité",
            type: "Type de champ",
            sectionName: "Sert à naviguer vers la section spécifiée",
            updateField: "Sert à mettre à jour le champ spécifié",
            selectOptions: [
                {
                    // Permet d'écrire en dur les valeurs d'un select
                    label: "Nom à afficher",
                    value: "Valeur à sauvegarder"
                },
            ],
            queryOptions: {
                sub_selection: [
                    // Sert aux sous-selection d'objet GraphQl
                    { 
                        key: "id",
                        label: "Nom à afficher",
                        action: "Vrai si constitue une action de selection" 
                    },
                ],
                selection_query: "Spécifie le nom de la query",
                selection_arguments: [
                    // Sert à passer des arguments à la query
                    { 
                        key: "id",
                        type: "Type de valeur",
                        update_from: "Si la valeur doit être prise depuis un autre champ",
                        required: "boolean"
                    }
                ],
            },
            mutationOptions: {
                selection_mutation: "Suffix de la mutation",
                mutation_arguments: [
                    {
                        key: "Identifiant",
                        type: "Type de valeur",
                        update_from: "Champs qui sert à savoir d'où provient la valeur de l'argument",
                    },
                ],           
            }, 
        },   
    ],
};
```

### Type de field

```
text : Type par défaut, in/out de chaine de caractères.
select: Liste de valeur prédéfinis à sélectionner.
input-select: Liste de valeur à sélectionner, possibilité d'écrire.
list: Liste d'objets à séléctionner, sélection multiple.
textarea: Comme pour text mais sous forme de standalone.
date: Possibilité d'écrire sous forme de texte ou avec un calendrier.
search: Utilise la view search pour naviguer vers de objets parentés.
```

## Lancer l'application
Il suffit de lancer la commande npm install, puis npm start, et d'ouvrir le navigateur à l'url localhost:4200
Pour pouvoir accéder à l'application, il faut se trouver dans la table Authentification.
Pour se faire, il faut d'abord créer une ligne dans la table Person, puis renseigner la table Authentification en hashant votre adresse mail et votre password.
Pour pouvoir hasher en sha256, vous pouvez utiliser ce site: https://md5decrypt.net/Sha256/
